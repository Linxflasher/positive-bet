import React from 'react';
import "./hint.scss";

const HintBox = (props) => (
    <div className={"hint-box " + props.position}>
        {props.children}
    </div>
);

export default HintBox;