import React, { Component } from "react";
import "./module.alert.scss";

import IcClose from "./IcClose/IcClose";

class Alert extends Component {
  constructor(props) {
    super(props);
    this.state = { isVisible: true };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      isVisible: !state.isVisible
    }));
  }

  render() {
    return (
      <div className={this.state.isVisible ? "alert " + this.props.status : "alert remove " + this.props.status}>
        <button className="alert-close-btn" onClick={this.handleClick}>
          <IcClose />
        </button>
        <p>{this.props.children}</p>
      </div>
    );
  }
}

export default Alert;
