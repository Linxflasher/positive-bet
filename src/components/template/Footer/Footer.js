import React from "react";
import "./footer.scss";

const Footer = () => (
  <div className="footer-wrapper">
    <footer className="footer">
      <div className="footer--copy">&copy; 2019 PositiveBet</div>
      <div className="footer--nav">
        <ul className="footer--nav--partnership">
          <li>
            <a className="f-link partners" href="/">
              Партнёрская программа
            </a>
          </li>
          <li>
            <a className="f-link" href="/">
              Клиентское приложение
            </a>
          </li>
        </ul>

        <ul className="footer--nav--tos">
          <li>
            <a className="f-link" href="/">
              Пользовательское соглашение
            </a>
          </li>
          <li>
            <a className="f-link" href="/">
              Политика конфиденциальности
            </a>
          </li>
          <li className="footer--nav--sup-link">
            <a className="f-link" href="/">
              Написать в поддержку
            </a>
          </li>
        </ul>
      </div>
    </footer>
  </div>
);

export default Footer;
