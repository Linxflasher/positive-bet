import React from "react";
import { Link } from "react-router-dom";
import "./header.lang.scss";

const HeaderLang = () => (
  <ul className="header--nav--lang">
    <li className="active">
      <Link to="">Ru</Link>
    </li>
    <li>
      <Link to="">En</Link>
    </li>
  </ul>
);

export default HeaderLang;
