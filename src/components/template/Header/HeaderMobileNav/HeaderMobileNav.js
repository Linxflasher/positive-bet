import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./header.mobile.nav.scss";

import Auth from "../../../pages/Auth/Auth";

class HeaderMobileNav extends Component {
  constructor(props) {
    super(props);
    this.state = { isModalLoginOpened: false, isModalRegOpened: false };

    this.handleModalLogin = this.handleModalLogin.bind(this);
    this.handleModalReg = this.handleModalReg.bind(this);
  }

  handleModalLogin() {
    this.setState(state => ({
      isModalLoginOpened: !state.isModalLoginOpened
    }));
  }

  handleModalReg() {
    this.setState(state => ({
      isModalRegOpened: !state.isModalRegOpened
    }));
  }

  render() {
    return (
      <React.Fragment>
        <Auth
          openLogin={this.state.isModalLoginOpened}
          closeLogin={this.handleModalLogin}
          openReg={this.state.isModalRegOpened}
          closeReg={this.handleModalReg}
        />
        <div
          className={
            this.props.isToggled
              ? "header--mobile-nav opened"
              : "header--mobile-nav"
          }
        >
          <ul className="main">
            <li className="active">
              <Link to="/live-forks">Live-вилки</Link>
            </li>
            <li>
              <Link to="/">Коридоры</Link>
            </li>
            <li>
              <Link to="/">Valuebets</Link>
            </li>
            <li>
              <Link to="/tariffs">Тарифы</Link>
            </li>
            <li>
              <Link to="/questions">Справка</Link>
            </li>
            <li>
              <Link to="/contacts">Контакты</Link>
            </li>
          </ul>
          {/* Авторизация */}
          {/* <ul className="auth">
            <li>
              <button onClick={this.handleModalLogin}>Вход</button>
            </li>
            <li>
              <button onClick={this.handleModalReg}>Регистрация</button>
            </li>
          </ul> */}
          {/* Профиль */}
          <ul className="profile">
            <li>
              <Link to="/profile">Личный кабинет</Link>
            </li>
            <li>
              <button>Выйти</button>
            </li>
          </ul>

          <ul className="lang">
            <li className="active">
              <Link to="">Ru</Link>
            </li>
            <li>
              <Link to="">En</Link>
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

export default HeaderMobileNav;
