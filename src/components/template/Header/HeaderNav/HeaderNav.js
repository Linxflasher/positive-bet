import React from "react";
import { Link } from "react-router-dom";
import "./header.nav.scss";

import HintBox from "../../../ui/HintBox/HintBox";

const HeaderNav = () => (
  <ul className="header--nav--main">
    <li>
      <Link to="/live-forks">Live-вилки</Link>
    </li>
    <li>
      <Link to="/">Коридоры</Link>
    </li>
    <li>
      <Link to="/">Valuebets</Link>
    </li>
    <li className="tariffs active">
      <Link to="/tariffs">Тарифы</Link>
      <HintBox position="bottom">
        Тарифные планы и общая стоимость подписок на Positivebet
      </HintBox>
    </li>
    <li>
      <Link to="/questions">Справка</Link>
    </li>
    <li>
      <Link to="/contacts">Контакты</Link>
    </li>
  </ul>
);

export default HeaderNav;
