import React from "react";
import { Link } from "react-router-dom";

import "./header.profile.scss";

const HeaderProfile = () => (
  <div className="header--nav--profile">
    <Link to="/profile">Личный кабинет</Link>
    <button className="logout-btn" />
  </div>
);

export default HeaderProfile;
