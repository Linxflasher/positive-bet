import React, { Component } from "react";
import "./header.auth.scss";

import Auth from "../../../pages/Auth/Auth";

class HeaderAuth extends Component {
  constructor(props) {
    super(props);
    this.state = { isModalLoginOpened: false, isModalRegOpened: false };

    this.handleModalLogin = this.handleModalLogin.bind(this);
    this.handleModalReg = this.handleModalReg.bind(this);
  }

  handleModalLogin() {
    this.setState(state => ({
      isModalLoginOpened: !state.isModalLoginOpened
    }));
  }

  handleModalReg() {
    this.setState(state => ({
      isModalRegOpened: !state.isModalRegOpened
    }));
  }

  render() {
    return (
      <React.Fragment>
        <Auth
          openLogin={this.state.isModalLoginOpened}
          closeLogin={this.handleModalLogin}
          openReg={this.state.isModalRegOpened}
          closeReg={this.handleModalReg}
        />
        <ul className="header--nav--auth">
          <li>
            <button onClick={this.handleModalLogin}>
              Вход
            </button>
          </li>
          <li className="active">
            <button onClick={this.handleModalReg}>Регистрация</button>
          </li>
        </ul>
      </React.Fragment>
    );
  }
}

export default HeaderAuth;
