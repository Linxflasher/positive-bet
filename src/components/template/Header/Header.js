import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./header.scss";
import Logo from "./logo.svg";

import HeaderNav from "./HeaderNav/HeaderNav";
import HeaderLang from "./HeaderLang/HeaderLang";
import HeaderAuth from "./HeaderAuth/HeaderAuth";
import HeaderProfile from "./HeaderProfile/HeaderProfile";
import HeaderMobileNav from "./HeaderMobileNav/HeaderMobileNav";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { isMobileToggled: false };

    this.handleMobileNav = this.handleMobileNav.bind(this);
  }

  handleMobileNav() {
    this.setState(state => ({
      isMobileToggled: !state.isMobileToggled
    }));

    !this.state.isMobileToggled
      ? document.body.classList.add("scrollLock")
      : document.body.classList.remove("scrollLock");
  }

  componentWillUnmount() {
    document.body.classList.remove("scrollLock");
  }

  render() {
    return (
      <div
        className={
          this.state.isMobileToggled
            ? "header-wrapper expanded"
            : "header-wrapper"
        }
      >
        <header className="header">
          <div className="header--main">
            <Link className="header--logo" to="/" title="Positive bet">
              <img src={Logo} alt="Positive Bet" />
            </Link>
            <nav className="header--nav">
              <HeaderNav />

              <HeaderLang />

              {/* <HeaderAuth /> */}

              <HeaderProfile />
            </nav>

            <button
              className={
                this.state.isMobileToggled ? "mobile-btn toggled" : "mobile-btn"
              }
              onClick={this.handleMobileNav}
            />
          </div>
          <HeaderMobileNav isToggled={this.state.isMobileToggled} />
        </header>
      </div>
    );
  }
}

export default Header;
