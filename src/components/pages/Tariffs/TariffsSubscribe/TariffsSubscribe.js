import React, { Component } from "react";
import "./tariffs.subscribe.scss";

import TariffsSubscribeToggler from "./TariffsSubscribeToggler/TariffsSubscribeToggler";

class TariffsSubscribe extends Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: false};

    this.handleToggle = this.handleToggle.bind(this);
  }
  

  handleToggle() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

  render() {
    return (
      <section className="subscription">
        <div className="subscription--title">
          <h1>Спеццена до конца июня</h1>
          <p>При оформлении подписки в июне, вы получите скидку 10%</p>
        </div>
        <TariffsSubscribeToggler click={this.handleToggle} class={this.state.isToggleOn ? "subscription--toggler active" : "subscription--toggler"} />
        
      </section>
    );
  }
}

export default TariffsSubscribe;
