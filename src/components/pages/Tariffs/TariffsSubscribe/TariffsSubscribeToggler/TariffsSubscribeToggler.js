import React from 'react';

const TariffsSubscribeToggler = (props) => (
    <button onClick={props.click} className={props.class}>
          <div className="slider" />
          <div className="subscribe">
            Подписка
          </div>
          <div className="subscribe-plus">
            <span>Подписка+</span>

            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M15.6905 9.52381V10.0238H16.1905H17.619C18.6762 10.0238 19.5 10.8476 19.5 11.9048C19.5 12.962 18.6762 13.7857 17.619 13.7857H16.1905H15.6905V14.2857V18.0952C15.6905 18.8667 15.0572 19.5 14.2857 19.5H11.1667V18.5714C11.1667 16.8667 9.79995 15.5 8.09524 15.5C6.39052 15.5 5.02381 16.8667 5.02381 18.5714V19.5H1.90476C1.13329 19.5 0.5 18.8667 0.5 18.0952L0.5 14.9762H1.42857C3.13329 14.9762 4.5 13.6095 4.5 11.9048C4.5 10.2 3.13329 8.83333 1.42857 8.83333H0.5L0.5 5.71429C0.5 4.94281 1.13329 4.30952 1.90476 4.30952L5.71429 4.30952H6.21429V3.80952V2.38095C6.21429 1.32376 7.03805 0.5 8.09524 0.5C9.15243 0.5 9.97619 1.32376 9.97619 2.38095L9.97619 3.80952V4.30952H10.4762L14.2857 4.30952C15.0572 4.30952 15.6905 4.94281 15.6905 5.71429V9.52381Z"
                stroke="#b2b2b2"
                strokeOpacity="0"
                fill="#4040FF"
              />
            </svg>
          </div>
        </button>
);

export default TariffsSubscribeToggler;