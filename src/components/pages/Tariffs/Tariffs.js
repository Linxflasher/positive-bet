import React, { Component } from "react";
import "./tariffs.scss";
import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

import TariffsSubscribe from "./TariffsSubscribe/TariffsSubscribe";
import TariffsList from "./TariffsList/TariffsList";
import TariffsMobileCarousel from "./TariffsMobileCarousel/TariffsMobileCarousel";

class Tariffs extends Component {
  constructor(props) {
    super(props);
    this.state = { isPassVisible: false };

    this.handlePassVisibility = this.handlePassVisibility.bind(this);
  }

  handlePassVisibility() {
    this.setState(state => ({
      isPassVisible: !state.isPassVisible
    }));
  }

  render() {
    return (
      <div className="page-wrapper">

        <Header />

        <main className="main-content tarif-page">
          <TariffsSubscribe />

          <TariffsList />

          <TariffsMobileCarousel/>

          <section className="notice">
            <p>
              Покупая подписку, вы принимаете условия пользовательского
              соглашения.
            </p>
            <p>
              <sup>*</sup>Минимальный период приостановки &mdash; любой,
              максимальный период приостановки &mdash; 3 месяца (по истечение 3
              месяцев, подписка автоматически возобновляется или используется
              доступная приостановка).
            </p>
          </section>
        </main>

        <Footer />
      </div>
    );
  }
}

export default Tariffs;
