import React from "react";
import "./tariffs.list.scss";
import {Link} from "react-router-dom";
import Card1 from "./TariffsIcons/Card1";
import Card2 from "./TariffsIcons/Card2";
import Card3 from "./TariffsIcons/Card3";
import Card4 from "./TariffsIcons/Card4";
import Dollar from "./TariffsIcons/Dollar";

const TariffsList = () => (
  <section className="subscription-list">
    <ul>
      <li className="subscription-list--item active">
        <div className="icon card-1">
          <Card1/>
        </div>
        <div className="date">День</div>
        <div className="pause">Без приостановок</div>
        <div className="price">
          <div className="dollars">
            8.9
            <Dollar/>
          </div>
          <div className="rubles">560&#8381;</div>
          <div className="of-price">&#36;9.9 / 620&#8381;</div>
        </div>
        <Link className="checkout-btn" to="/payment">Оформить</Link>
      </li>
      <li className="subscription-list--item">
        <div className="icon card-2">
          <Card2/>
        </div>
        <div className="date">Неделя</div>
        <div className="pause">1 приостановка</div>
        <div className="price">
          <div className="dollars">
            27
            <Dollar/>
          </div>
          <div className="rubles">1 699&#8381;</div>
          <div className="of-price">&#36;29.9 / 1 875&#8381;</div>
        </div>
        <Link className="checkout-btn" to="/payment">Оформить</Link>
      </li>
      <li className="subscription-list--item best-offer">
        <div className="icon card-3">
          <Card3/>
        </div>
        <div className="date">Месяц</div>
        <div className="pause">2 приостановки</div>
        <div className="price">
          <div className="dollars">
            89
            <Dollar/>
          </div>
          <div className="rubles">5 599&#8381;</div>
          <div className="of-price">&#36;99 / 6 211&#8381;</div>
        </div>
        <Link className="checkout-btn" to="/payment">Оформить</Link>
      </li>
      <li className="subscription-list--item">
        <div className="icon card-4">
          <Card4/>
        </div>
        <div className="date">Три месяца</div>
        <div className="pause">4 приостановки</div>
        <div className="price">
          <div className="dollars">
            224
            <Dollar/>
          </div>
          <div className="rubles">14 092&#8381;</div>
          <div className="of-price">&#36;249 / 15 622&#8381;</div>
        </div>
        <Link className="checkout-btn" to="/payment">Оформить</Link>
      </li>
    </ul>
  </section>
);

export default TariffsList;
