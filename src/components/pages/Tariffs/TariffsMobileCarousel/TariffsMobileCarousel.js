import React from "react";
import { Carousel } from "react-responsive-carousel";
import {Link} from "react-router-dom";
import "./mobile.carousel.scss";
import Card1 from "../TariffsList/TariffsIcons/Card1";
import Card2 from "../TariffsList/TariffsIcons/Card2";
import Card3 from "../TariffsList/TariffsIcons/Card3";
import Card4 from "../TariffsList/TariffsIcons/Card4";
import Dollar from "../TariffsList/TariffsIcons/Dollar";

const TariffsMobileCarousel = () => (
  <Carousel className="tariffs-mobile" showThumbs={false} showArrows={false} showStatus={false} centerMode={true}>
    <div className="subscription-list--item">
      <div className="icon card-1">
        <Card1/>
      </div>
      <div className="date">День</div>
      <div className="pause">Без приостановок</div>
      <div className="price">
        <div className="dollars">
          8.9
          <Dollar/>
        </div>
        <div className="rubles">560&#8381;</div>
        <div className="of-price">&#36;9.9 / 620&#8381;</div>
      </div>
      <Link to="/payment" className="checkout-btn">Оформить</Link>
    </div>
    <div className="subscription-list--item">
      <div className="icon card-2">
        <Card2/>
      </div>
      <div className="date">Неделя</div>
      <div className="pause">1 приостановка</div>
      <div className="price">
        <div className="dollars">
          27
          <Dollar/>
        </div>
        <div className="rubles">1 699&#8381;</div>
        <div className="of-price">&#36;29.9 / 1 875&#8381;</div>
      </div>
      <Link to="/payment" className="checkout-btn">Оформить</Link>
    </div>
    <div className="subscription-list--item best-offer">
      <div className="icon card-3">
        <Card3/>
      </div>
      <div className="date">Месяц</div>
      <div className="pause">2 приостановки</div>
      <div className="price">
        <div className="dollars">
          89
          <Dollar/>
        </div>
        <div className="rubles">5 599&#8381;</div>
        <div className="of-price">&#36;99 / 6 211&#8381;</div>
      </div>
      <Link to="/payment" className="checkout-btn">Оформить</Link>
    </div>
    <div className="subscription-list--item">
      <div className="icon card-4">
        <Card4/>
      </div>
      <div className="date">Три месяца</div>
      <div className="pause">4 приостановки</div>
      <div className="price">
        <div className="dollars">
          224
          <Dollar/>
        </div>
        <div className="rubles">14 092&#8381;</div>
        <div className="of-price">&#36;249 / 15 622&#8381;</div>
      </div>
      <Link to="/payment" className="checkout-btn">Оформить</Link>
    </div>
  </Carousel>
);

export default TariffsMobileCarousel;
