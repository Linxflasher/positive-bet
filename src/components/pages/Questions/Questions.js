import React, { Component } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import "./questions.scss";
import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = { isContentsVisible: false };

    this.handleContentsVisibility = this.handleContentsVisibility.bind(this);
  }

  handleContentsVisibility() {
    this.setState(state => ({
      isContentsVisible: !state.isContentsVisible
    }));
  }

  render() {
    return (
      <div className="page-wrapper">
        <Header />
        <div className="faq-page">
          <button className="mobile-content-switcher" onClick={this.handleContentsVisibility}>{this.state.isContentsVisible ? "К материалу" : "Содержание"}</button>
          <div className="questions" style={{display: this.state.isContentsVisible ? "block" : "none"}}>
            <PerfectScrollbar options={{ suppressScrollX: true, maxScrollbarLength: 32 }}>
              <ul className="faq-list">
                <li className="active">Букмекерская вилка &mdash; что это?</li>
                <li>Часто задаваемые вопросы от новичков</li>
                <li>Выбираем букмекерские конторы для новичков</li>
                <li>Толковый словарь терминов</li>
                <li>Словарь переводов иностранных терминов ставок</li>
                <li>
                  Что такое "линия ставок"?
                  <br />
                  Виды ставок
                </li>
                <li>Линия ставок 1Х2, Moneyline (денежная линия)</li>
                <li>Ставки с форой</li>
                <li>Ставки на тотал</li>
                <li>Азиатский гандикап (азиатская фора)</li>
                <li>Азиатский тотал</li>
                <li>Европейский гандикап (3Way)</li>
                <li>Ставки с перевесом</li>
                <li>
                  Skrill (Moneybookers) – платёжная система для работы с БК
                </li>
                <li>Почему букмекеры режут вилочников? Как этого избежать?</li>
                <li>Как формируются коэффициенты, маржа букмекера?</li>
                <li>Что такое "биржа ставок"?</li>
                <li>Букмекерская вилка &mdash; что это?</li>
                <li>Часто задаваемые вопросы от новичков</li>
                <li>Выбираем букмекерские конторы для новичков</li>
                <li>Толковый словарь терминов</li>
                <li>Словарь переводов иностранных терминов ставок</li>
                <li>
                  Что такое "линия ставок"?
                  <br />
                  Виды ставок
                </li>
                <li>Линия ставок 1Х2, Moneyline (денежная линия)</li>
                <li>Ставки с форой</li>
                <li>Ставки на тотал</li>
                <li>Азиатский гандикап (азиатская фора)</li>
                <li>Азиатский тотал</li>
                <li>Европейский гандикап (3Way)</li>
                <li>Ставки с перевесом</li>
                <li>
                  Skrill (Moneybookers) – платёжная система для работы с БК
                </li>
                <li>Почему букмекеры режут вилочников? Как этого избежать?</li>
                <li>Как формируются коэффициенты, маржа букмекера?</li>
                <li>Что такое "биржа ставок"?</li>
              </ul>
            </PerfectScrollbar>
          </div>
          <div className="answers" style={{display: this.state.isContentsVisible ? "none" : "block"}}>
            <h1>
              Часто задаваемые вопросы
              <br /> от новичков
            </h1>
            <p>
              Данная статья посвящается часто задаваемым вопросам касаемо ставок
              на вилки в букмекерских конторах, сокращенно "бк". В ней мы
              ответим на самые волнующие вопросы новичков: как зарабатывать на
              букмекерских вилках, как выводить деньги с букмекерских контор и
              т.д. Также мы дадим несколько важных советов, которые помогут
              минимизировать потери на первых этапах.
            </p>
            <p>
              Если у вас возникнут вопросы, и вы не найдете на них ответы в
              данном руководстве, пишите нам! Мы с радостью ответим.
            </p>

            <div className="answer--item">
              <h2>1. Что такое букмекерские вилки или арбитражные ситуации?</h2>
              <p>
                Букмекерские вилки (арбитражная ситуация, англ. arbitrage, англ.
                surebet) — это возможность сделать ставки на все возможные
                результаты состязания в разных букмекерских конторах и получить
                прибыль независимо от исхода состязания. Размер прибыли
                находится в пределах нескольких процентов от суммы ставок.
                (wikipedia.org)
              </p>
            </div>
            <div className="answer--item">
              <h2>2. Легально ли делать ставки на вилки?</h2>
              <p>
                Делать ставки на вилки в букмекерских конторах на любые
                спортивные события абсолютно легально, это ничем не отличается
                обычных ставок. Букмекер предлагает цену, а вы соглашаетесь с
                ней, размещая ставку. Сервисы поиска лучших коэффициентов и
                вилочные сервисы подобны сервисам сравнения цен в интернете.
                Заработок на букмекерских конторах при помощи ставок на
                букмекерские вилки совершенно легален!
              </p>
            </div>
            <div className="answer--item">
              <h2>3. Если это очень выгодно, почему все не занимаются этим?</h2>
              <p>
                Во-первых, не все об этом знают. Во-вторых, не все верят, что
                можно зарабатывать на вилках в ставках на спорт. В третьих, у
                каждого способа заработка есть свои плюсы и минусы. Каждый
                выбирает, что ему интересно на фоне того, сколько он может
                заработать на этом. В целом можно сказать, что вилки неплохо
                подходят для дополнительного заработка. При умеренном риске
                рентабельность капитала достаточно высокая.
              </p>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Questions;
