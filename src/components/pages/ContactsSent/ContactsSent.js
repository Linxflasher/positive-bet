import React from "react";
import "./contacts.sent.scss";
import {Link} from "react-router-dom";
import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

import Letter from "./ic-letter.svg";

const ContactsSent = () => (
  <div className="page-wrapper">
    <Header />
    <div className="contacts-sent">
      <div className="wrapper">
          <div className="img">
            <img src={Letter} alt="" />
          </div>
          <h1>Сообщение в службу поддержки успешно отправлено!</h1>
          <p>
            Спасибо, что обратились в нашу службу поддержки. Обычно наши специалисты
            отвечают в течение трех часов.{" "}
          </p>
          <Link className="back-link" to="/contacts">Назад к форме обратной связи</Link>
      </div>
    </div>
    <Footer />
  </div>
);

export default ContactsSent;
