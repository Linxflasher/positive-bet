import React from "react";
import { Link } from "react-router-dom";
import "./main.plugins.scss";
import pluginImg from "./plugin-image.png";

const MainPlugins = () => (
  <div className="plugins">
    <div className="plugins--image">
      <img src={pluginImg} alt="" />
    </div>
    <div className="plugins--info">
      <h3>Отслеживайте вилки в нашем плагине для браузера BetExtension</h3>
      <p>Во всех популярных браузерах</p>
      <ul className="browsers">
        <li className="browser--item chrome" />
        <li className="browser--item yandex" />
        <li className="browser--item firefox" />
        <li className="browser--item opera" />
      </ul>
      <div className="btn-group">
        <Link to="" className="link-btn details">
          Подробнее
        </Link>
        <Link to="" className="link-btn subscribe">
          Подписка+
        </Link>
      </div>
    </div>
  </div>
);

export default MainPlugins;
