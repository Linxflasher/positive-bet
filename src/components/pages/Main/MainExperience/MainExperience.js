import React from "react";
import "./main.exp.scss";
import expBg from "./exp-bg.png";

const MainExperience = () => (
  <div className="experience">
    <img src={expBg} alt="" />
    <div className="experience-block">
      <span>Наш опыт</span>
      <p>
        За 6 лет работы PositiveBet стал эталоном сканера, работающего с вилками
        в реальном времени.{" "}
      </p>
    </div>
  </div>
);

export default MainExperience;
