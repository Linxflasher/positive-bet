import React from 'react';
import "./main.scss";
import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";
import MainHero from "./MainHero/MainHero";
import MainSports from "./MainSports/MainSports";
import MainExperience from "./MainExperience/MainExperience";
import MainFeatures from "./MainFeatures/MainFeatures";
import MainWiki from "./MainWiki/MainWiki";
import MainPlugins from "./MainPlugins/MainPlugins";
import MainRegister from "./MainRegister/MainRegister";

const Main = () => (
    <div className="main-page">
        <Header/>
        <MainHero/>
        <MainSports/>
        <MainExperience/>
        <MainFeatures/>
        <MainWiki/>
        <MainPlugins/>
        <MainRegister/>
        <Footer/>
    </div>
);

export default Main;