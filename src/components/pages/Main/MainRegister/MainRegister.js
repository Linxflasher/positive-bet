import React from "react";
import "./main.register.scss";

const MainRegister = () => (
  <div className="register">
    <div className="register--desc">
      <h4>Регистрируйтесь прямо сейчас</h4>
      <p>
        Регистрируйтесь, для посмотра всех возможностей сервиса: кроме вилок с
        доходностью не более 1%,{" "}
      </p>
      <p>вам станет доступна вся функциольность сервиса: </p>
      <ul>
        <li>Фильтры</li>
        <li>Персональные настройки</li>
        <li>Сортировка событий</li>
      </ul>
    </div>
    <div className="register--form">
      <form className="form-reg" action="">
        <div className="input-rows error">
          <div className="form-row error">
            <input
              className=""
              type="email"
              placeholder="E-mail"
              value="test@tttre"
            />
            <div className="error-msg">неправильный формат E-mail</div>
          </div>
          <div className="form-row password-text error">
            <input type="password" placeholder="Пароль" value="12345678" />
            <span>Мин. 4 символа</span>
          </div>
          <div className="form-row password-text error">
            <input type="password" placeholder="Повторите Пароль" value="12345678" />
            <div className="error-msg">Введенные пароли не совпадают</div>
          </div>
        </div>

        <div className="agreement">
          <div className="form-row checkbox error">
            <input type="checkbox" id="user-agr" name="user-agr" />
            <label htmlFor="user-agr">
              Я принимаю условия&nbsp;
              <a href="/">Пользовательского соглашения</a>
            </label>
            <div className="error-msg">Обязательно для регистрации</div>
          </div>
          <div className="form-row checkbox error">
            <input type="checkbox" id="data-agr" name="data-agr" />
            <label htmlFor="data-agr">
              Я даю своё согласие на сбор, обработку и хранение моих
              персональных данных
            </label>
            <div className="error-msg">Обязательно для регистрации</div>
          </div>
        </div>

        <button className="submit-btn" type="submit">
          Зарегистрироваться
        </button>
      </form>
    </div>
  </div>
);

export default MainRegister;
