import React, { Component } from "react";
import "./main.wiki.scss";
import videoPoster from "./video-poster.jpg";

class MainWiki extends Component {
  state = {
    videoPlays: false
  };

  playVideo() {
    this.refs.vidRef.play();
    this.setState({
      videoplays: !this.state.videoPlays
    });
  }

  render() {
    return (
      <div className="wiki">
        <div className="wiki--block">
          <div className="wiki--block--description">
            <h1>Я хочу попробовать прямо сейчас</h1>
            <p>
              Посмотрите нашу краткую инструкцию для старта работы с сканером
              вилок. Наша мини-эциклопедия будет полезна как новичку, так и
              профессиональному вилочнику.
            </p>
            <button className="link-btn check">Справка</button>
          </div>
          <div className="wiki--block--video">
            <button
              onClick={this.playVideo.bind(this)}
              className={
                this.state.videoplays
                  ? "play-video-btn playing"
                  : "play-video-btn"
              }
            />
            <video ref="vidRef" poster={videoPoster} controls>
              <source
                src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                type="video/mp4"
              />
              Этот браузер не поддерживает video тэг.
            </video>
          </div>
        </div>
      </div>
    );
  }
}

export default MainWiki;
