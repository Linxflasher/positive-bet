import React from 'react';
import {Link} from "react-router-dom";
import "./main.hero.scss";
import heroBg from "./hero-bg.jpg";

const MainHero = () => (
    <div className="hero">
        <img src={heroBg} alt="PositiveBet - сервис по поиску live-вилок" />
        <div className="hero--block">
            <h1>Добро пожаловать на сервис<br/> поиска live вилок PositiveBet</h1>
            <p>Мы первый российский сервис по работе с событиями во время спортивных матчей.</p>
            <p>Специализируемся только на live-вилках и уверенно заявляем, что являемся одними из лучших в этом направлении.</p>
            <div className="btn-group">
                <Link to="" className="link-btn about">О сервисе</Link>
                <Link to="" className="link-btn reg">Регистрация</Link>
            </div>
        </div>
    </div>
);

export default MainHero;