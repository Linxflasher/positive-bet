import React from "react";
import {Link} from "react-router-dom";
import "./main.features.scss";

import liveForkImg from "./live-fork.png";
import koridorImg from "./koridor.png";
import valueBetsImg from "./valuebets.png";

const MainFeatures = () => (
  <ul className="features-list">
    <li>
      <img src={liveForkImg} alt="" />
      <div className="title">Live-вилки</div>
      <p className="text">
        Стратегия учета разной оценки и разных коэффициентов одного и того же
        спортивного события
      </p>
      <Link to="" className="link-btn btn">Live-вилки</Link>
    </li>
    <li>
      <img src={koridorImg} alt="" />
      <div className="title">Коридоры</div>
      <p className="text">Стратегия, выбирающая противоположные результаты спортивных событий</p>
      <Link to="" className="link-btn btn">Коридоры</Link>
    </li>
    <li>
      <img src={valueBetsImg} alt="" />
      <div className="title">Valuebets</div>
      <p className="text">
        Стратегия выбора события с недооцененным исходом, завышенные по
        сравнению со средним значением
      </p>
      <Link to="" className="link-btn btn">Ставки Valuebets</Link>
    </li>
  </ul>
);

export default MainFeatures;
