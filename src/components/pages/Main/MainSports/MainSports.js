import React from 'react';
import {Link} from "react-router-dom";
import "./main.sports.scss";

import imgSport1 from "./sport-1.png";
import imgSport2 from "./sport-2.png";
import imgSport3 from "./sport-3.png";
import imgSport4 from "./sport-4.png";
import imgSport5 from "./sport-5.png";
import imgSport6 from "./sport-6.png";
import imgSport7 from "./sport-7.png";
import imgSport8 from "./sport-8.png";
import imgSport9 from "./sport-9.png";
import imgSport10 from "./sport-10.png";

const MainSports = () => (
    <div className="sports">
        <div className="sports-row">
            <img src={imgSport1} alt=""/>
            <img src={imgSport2} alt=""/>
            <img src={imgSport3} alt=""/>
            <img src={imgSport4} alt=""/>
            <img src={imgSport5} alt=""/>
        </div>
        <div className="sports-info">
            <p>Мы работаем с вилками по 10 наиболее популярным видам спорта, включая футбол, волейбол, теннис, баскетбол и хоккей.</p>
            <Link to="/" className="link-btn sports-link">Виды спорта и лиги</Link>
        </div>
        <div className="sports-row">
            <img src={imgSport6} alt=""/>
            <img src={imgSport7} alt=""/>
            <img src={imgSport8} alt=""/>
            <img src={imgSport9} alt=""/>
            <img src={imgSport10} alt=""/>
        </div>
    </div>
);

export default MainSports;