import React, { Component } from 'react';
import "./live-forks.side.filter.scss";

class LiveForksSideFilter extends Component {
    state = {  }
    render() {
        return (
            <div className="live-forks--side--filter">
                <form>
                    <div className="title">
                        <h4>Мои фильтры</h4>
                        <button className="btn btn-add"></button>
                    </div>
                    <ul className="list">
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-1" name="filter-1" />
                                <label htmlFor="filter-1">
                                    <span>Футбол Parimatch</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox red">
                                <input type="checkbox" id="filter-2" name="filter-2" />
                                <label htmlFor="filter-2">
                                    <span>Favbet New</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox blue">
                                <input type="checkbox" id="filter-3" name="filter-3" />
                                <label htmlFor="filter-3">
                                    <span>Только Vbet</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox blue">
                                <input type="checkbox" id="filter-4" name="filter-4" />
                                <label htmlFor="filter-4">
                                    <span>Не больше минуты</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox blue">
                                <input type="checkbox" id="filter-5" name="filter-5" />
                                <label htmlFor="filter-5">
                                    <span>Betcity</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-6" name="filter-6" />
                                <label htmlFor="filter-6">
                                    <span>LigaStavok</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-7" name="filter-7" />
                                <label htmlFor="filter-7">
                                    <span>Настольный теннис</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-8" name="filter-8" />
                                <label htmlFor="filter-8">
                                    <span>Теннис</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-9" name="filter-9" />
                                <label htmlFor="filter-9">
                                    <span>Все на тоталы</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox red">
                                <input type="checkbox" id="filter-10" name="filter-10" />
                                <label htmlFor="filter-10">
                                    <span>Победы первого</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-11" name="filter-11" />
                                <label htmlFor="filter-11">
                                    <span>Длинные вилки</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-12" name="filter-12" />
                                <label htmlFor="filter-12">
                                    <span>Favbet</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox green">
                                <input type="checkbox" id="filter-13" name="filter-13" />
                                <label htmlFor="filter-13">
                                    <span>Parimatch все</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-14" name="filter-14" />
                                <label htmlFor="filter-14">
                                    <span>Быстрые вилки</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div className="form-row checkbox">
                                <input type="checkbox" id="filter-15" name="filter-15" />
                                <label htmlFor="filter-15">
                                    <span>Теннис победы</span>
                                </label>
                            </div>
                        </li>
                    </ul>
                </form>
            </div>
        );
    }
}

export default LiveForksSideFilter;