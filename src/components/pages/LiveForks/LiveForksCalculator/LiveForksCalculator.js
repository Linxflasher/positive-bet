import React, { Component } from "react";
import "./live-forks.calculator.scss";
import IcTableTennis from "../LiveForksGroup/IcTableTennis";
import IcWarning from "../LiveForksGroup/IcWarning";
import IcCancel from "../LiveForksGroup/IcCancel";
import IcUpdate from "./IcUpdate";
import { Dropdown } from "semantic-ui-react";

const bookmakersList = [
  {
    key: "1",
    text: "Favbet",
    value: "1"
  },
  {
    key: "2",
    text: "Betcity",
    value: "2"
  },
  {
    key: "3",
    text: "Liga Stavok",
    value: "3"
  },
  {
    key: "4",
    text: "Parimatch",
    value: "4"
  }
];

const currencyList = [
  {
    key: "1",
    text: "USD",
    value: "1"
  },
  {
    key: "2",
    text: "RUB",
    value: "2"
  },
  {
    key: "3",
    text: "EUR",
    value: "3"
  },
  {
    key: "4",
    text: "GBP",
    value: "4"
  }
];

const roundingOptions = [
  {
    key: "1",
    text: "1",
    value: "1"
  },
  {
    key: "2",
    text: "2",
    value: "2"
  },
  {
    key: "3",
    text: "3",
    value: "3"
  },
  {
    key: "4",
    text: "4",
    value: "4"
  }
];

class LiveForksCalculator extends Component {
  state = {};
  render() {
    return (
      <div className="live-forks--calculator">
        <div className="live-forks--calculator--heading">
          <div className="percent">
            27.06<sub>%</sub>
          </div>
          <div className="sport">
            <IcTableTennis />
            <span>Настольный теннис</span>
          </div>
          <div className="btn-group">
            <button className="btn-warning">
              <IcWarning />
            </button>
            <button className="btn-cancel">
              <IcCancel />
            </button>
          </div>
        </div>
        <div className="live-forks--calculator--tables">
          <table>
            <thead>
              <tr>
                <th className="th-bookmaker">Букмекер</th>
                <th className="th-bet">Ставка</th>
                <th className="th-rate">Коэфф.</th>
                <th className="th-update">
                  <button className="btn-update">
                    <IcUpdate />
                  </button>
                </th>
                <th className="th-check">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="all-1" name="all-1" />
                    <label htmlFor="all-1" />
                  </div>
                </th>
                <th className="th-sum">Сумма</th>
                <th className="th-currency">Валюта</th>
                <th className="th-allocated">Расп.</th>
                <th className="th-fixed">Фикс.</th>
                <th className="th-income">Доход</th>
                <th className="th-rounding">Округ.</th>
                <th className="th-close" />
                <th className="th-plugin" />
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="td-bookmaker">
                  <Dropdown
                    defaultValue="1"
                    placeholder=""
                    selection
                    options={bookmakersList}
                  />
                </td>
                <td className="td-bet">П1</td>
                <td className="td-rate">
                  <div className="form-row">
                    <input type="number" value="2" placeholder="" />
                  </div>
                </td>
                <td className="td-update">
                  <button className="btn-update single">
                    <IcUpdate />
                  </button>
                </td>
                <td className="td-check">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="10" name="10" />
                    <label htmlFor="10" />
                  </div>
                </td>
                <td className="td-sum">
                  <div className="form-row">
                    <input type="number" value="69" placeholder="" />
                  </div>
                </td>
                <td className="td-currency">
                  <Dropdown
                    defaultValue="1"
                    placeholder=""
                    selection
                    options={currencyList}
                  />
                </td>
                <td className="td-allocated">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="al-10" name="al-10" />
                    <label htmlFor="al-10" />
                  </div>
                </td>
                <td className="td-fixed">
                  <div className="form-row radio">
                    <input type="radio" id="f-10" name="fixed-1" />
                    <label htmlFor="f-10" />
                  </div>
                </td>
                <td className="td-income">
                  <div className="form-row">
                    <input type="number" value="38" placeholder="" />
                  </div>
                </td>
                <td className="td-rounding">
                  <Dropdown
                    defaultValue="1"
                    placeholder=""
                    selection
                    options={roundingOptions}
                  />
                </td>
                <td className="td-close">
                  <button className="btn-close" />
                </td>
                <td className="td-plugin">
                  <button className="btn-plugin" />
                </td>
              </tr>
              <tr>
                <td className="td-event" colSpan="13">
                  <div className="event">
                    <button className="ev-copy" />
                    <span className="ev-part">Тимо Болл - Ю Жоу</span>
                    <span className="ev-divider"></span>
                    <span className="ev-name">
                       Открытый чемпионат Гонконга
                    </span>
                    <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                  </div>
                </td>
              </tr>
              <tr>
                <td className="td-bookmaker">
                  <Dropdown
                    defaultValue="2"
                    placeholder=""
                    selection
                    options={bookmakersList}
                  />
                </td>
                <td className="td-bet">П2</td>
                <td className="td-rate">
                  <div className="form-row">
                    <input type="number" value="4.75" placeholder="" />
                  </div>
                </td>
                <td className="td-update">
                  <button className="btn-update single">
                    <IcUpdate />
                  </button>
                </td>
                <td className="td-check">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="20" name="20" />
                    <label htmlFor="20" />
                  </div>
                </td>
                <td className="td-sum">
                  <div className="form-row">
                    <input type="number" value="31" placeholder="" />
                  </div>
                </td>
                <td className="td-currency">
                  <Dropdown
                    defaultValue="1"
                    placeholder=""
                    selection
                    options={currencyList}
                  />
                </td>
                <td className="td-allocated">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="al-20" name="al-20" />
                    <label htmlFor="al-20" />
                  </div>
                </td>
                <td className="td-fixed">
                  <div className="form-row radio">
                    <input type="radio" id="f-20" name="fixed-1" />
                    <label htmlFor="f-20" />
                  </div>
                </td>
                <td className="td-income">
                  <div className="form-row">
                    <input type="number" value="38" placeholder="" />
                  </div>
                </td>
                <td className="td-rounding">
                  <Dropdown
                    defaultValue="1"
                    placeholder=""
                    selection
                    options={roundingOptions}
                  />
                </td>
                <td className="td-close">
                  <button className="btn-close" />
                </td>
                <td className="td-plugin">
                  <button className="btn-plugin" />
                </td>
              </tr>
              <tr>
                <td className="td-event" colSpan="13">
                  <div className="event">
                    <button className="ev-copy" />
                    <span className="ev-part">Тимо Болл - Ю Жоу</span>
                    <span className="ev-divider"></span>
                    <span className="ev-name">
                      Открытый чемпионат Гонконга
                    </span>
                    <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                  </div>
                </td>
              </tr>
              <tr>
                <td className="td-bks" colSpan="5">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="all-bk-0" name="all-bk-0" />
                    <label htmlFor="all-bk-0">только мои конторы</label>
                  </div>
                </td>
                <td className="td-sum">
                  <div className="form-row">
                    <input type="number" value="100" placeholder="" />
                  </div>
                </td>
                <td className="td-currency">
                  <Dropdown
                    defaultValue="1"
                    placeholder=""
                    selection
                    options={currencyList}
                  />
                </td>
                <td className="td-allocated" />
                <td className="td-fixed">
                  <div className="form-row radio">
                    <input type="radio" id="f-30" name="fixed-1" />
                    <label htmlFor="f-30" />
                  </div>
                </td>
                <td className="td-empty" colSpan="4" />
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default LiveForksCalculator;
