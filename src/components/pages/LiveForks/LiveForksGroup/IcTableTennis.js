import React from "react";

const IcTableTennis = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M14 7C14 12 9 12 9 12H6C6 12 1 12 1 7C1 2 5 0 7.5 0C10 0 14 2 14 7ZM16.5 12C17.9 12 19 13.1 19 14.5C19 15.9 17.9 17 16.5 17C15.1 17 14 15.9 14 14.5C14 13.1 15.1 12 16.5 12ZM5 13C5 13 6 14 6 15V18.5C6 19.3 6.7 20 7.5 20C8.3 20 9 19.3 9 18.5V15C9 14 10 13 10 13H5Z"
      fill="#708DFF"
    />
  </svg>
);

export default IcTableTennis;
