import React, { Component } from "react";
import "./live-forks.group.scss";

import IcStack from "./IcStack";
import IcCalc from "./IcCalc";
import IcWarning from "./IcWarning";
import IcCancel from "./IcCancel";
import IcCopy from "./IcCopy";
import IcTableTennis from "./IcTableTennis";

class LiveForksGroups extends Component {
  state = {};
  render() {
    return (
      <div className="live-forks--group">
        {/* Группа */}
        {/* Класс active для активной группы */}
        <div className="group--item active">
          {/* Шапка группы */}
          <div className="group--item--heading">
            <div className="left">
              <div className="percent">27.06%</div>
              <div className="sport">
                <IcTableTennis />
                <span className="time">1 с</span>
              </div>
              <div className="forks">
                <button className="btn-copy">
                  <IcStack />
                </button>
                <span>2 вилки</span>
              </div>
            </div>
            <div className="right">
              <button className="btn-calc active">
                <IcCalc />
              </button>
              <button className="btn-warn">
                <IcWarning />
              </button>
              <button className="btn-cancel">
                <IcCancel />
              </button>
            </div>
          </div>
          {/* Шапка группы End */}
          {/* Таблица */}
          <table className="group--item--table">
            <tbody>
              <tr>
                <td className="provider">Favbet</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">Тимо Болл - Ю Жоу</p>
                  <span className="event-name">
                    Открытый чемпионат Гонконга 1:2
                  </span>
                  <span className="event-score">(9:11,9:11,11:9,1:0)</span>
                </td>
                <td className="bet">П1</td>
                <td className="dynamics fall">
                  <span>2.00</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
              <tr>
                <td className="provider">Betcity</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">Boll Timo - Zhou Yu</p>
                  <span className="event-name">
                    Table Tennis. Men. ITTF World Tour. Hong Kong{" "}
                  </span>
                </td>
                <td className="bet">П2</td>
                <td className="dynamics rise">
                  <span>4.35</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        {/* Группа End */}
        {/* Группа */}
        {/* Класс increase при повышении */}
        <div className="group--item increase">
          {/* Шапка группы */}
          <div className="group--item--heading">
            <div className="left">
              <div className="percent">22.21%</div>
              <div className="sport">
                <IcTableTennis />
                <span className="time">14 с</span>
              </div>
              <div className="forks">
                <button className="btn-copy">
                  <IcStack />
                </button>
                <span>2 вилки</span>
              </div>
            </div>
            <div className="right">
              <button className="btn-calc">
                <IcCalc />
              </button>
              <button className="btn-warn">
                <IcWarning />
              </button>
              <button className="btn-cancel">
                <IcCancel />
              </button>
            </div>
          </div>
          {/* Шапка группы End */}
          {/* Таблица */}
          <table className="group--item--table">
            <tbody>
              <tr>
                <td className="provider">Favbet</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">
                    Бузарнеску Михаела vs Гергес Юлия
                  </p>
                  <span className="event-name">Теннис. WTA. Рим. Грунт</span>
                  <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                </td>
                <td className="bet">П1</td>
                <td className="dynamics fall">
                  <span>2.00</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
              <tr>
                <td className="provider">Betcity</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">
                    Михаэла Бузэрнеску vs Юлия Гёргес
                  </p>
                  <span className="event-name">WTA Рим - Грунтовый</span>
                  <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                </td>
                <td className="bet">П2</td>
                <td className="dynamics rise">
                  <span>4.70</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        {/* Группа End */}
        {/* Группа */}
        <div className="group--item">
          {/* Шапка группы */}
          <div className="group--item--heading">
            <div className="left">
              <div className="percent">18.34%</div>
              <div className="sport">
                <IcTableTennis />
                <span className="time">23 с</span>
              </div>
              <div className="forks">
                <button className="btn-copy">
                  <IcStack />
                </button>
                <span>33 вилки</span>
              </div>
            </div>
            <div className="right">
              <button className="btn-calc">
                <IcCalc />
              </button>
              <button className="btn-warn">
                <IcWarning />
              </button>
              <button className="btn-cancel">
                <IcCancel />
              </button>
            </div>
          </div>
          {/* Шапка группы End */}
          {/* Таблица */}
          <table className="group--item--table">
            <tbody>
              <tr>
                <td className="provider">Parimatch</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">
                    Бузарнеску Михаела vs Гергес Юлия
                  </p>
                  <span className="event-name">Теннис. WTA. Рим. Грунт</span>
                  <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                </td>
                <td className="bet">П1(8 гейм) (1 сет)</td>
                <td className="dynamics fall">
                  <span>2.65</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
              <tr>
                <td className="provider">Vbet</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">
                    Михаэла Бузэрнеску vs Юлия Гёргес
                  </p>
                  <span className="event-name">WTA Рим - Грунтовый</span>
                  <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                </td>
                <td className="bet">П2(8 гейм) (1 сет)</td>
                <td className="dynamics rise">
                  <span>2.70</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        {/* Группа End */}
        {/* Группа */}
        {/* Класс decrease при понижении */}
        <div className="group--item decrease">
          {/* Шапка группы */}
          <div className="group--item--heading">
            <div className="left">
              <div className="percent">25.45%</div>
              <div className="sport">
                <IcTableTennis />
                <span className="time">24 с</span>
              </div>
              <div className="forks">
                <button className="btn-copy">
                  <IcStack />
                </button>
                <span>5 вилок</span>
              </div>
            </div>
            <div className="right">
              <button className="btn-calc">
                <IcCalc />
              </button>
              <button className="btn-warn">
                <IcWarning />
              </button>
              <button className="btn-cancel">
                <IcCancel />
              </button>
            </div>
          </div>
          {/* Шапка группы End */}
          {/* Таблица */}
          <table className="group--item--table">
            <tbody>
              <tr>
                <td className="provider">Sportingbull</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">
                    Михаэла Бузэрнеску vs Юлия Гёргес
                  </p>
                  <span className="event-name">(WTA Рим - Грунтовый)</span>
                  <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                </td>
                <td className="bet">П1(8 гейм) (1 сет)</td>
                <td className="dynamics fall">
                  <span>1.35</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
              <tr>
                <td className="provider">Vbet</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">
                    Михаэла Бузэрнеску vs Юлия Гёргес
                  </p>
                  <span className="event-name">(WTA Рим - Грунтовый)</span>
                  <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                </td>
                <td className="bet">П2(8 гейм) (1 сет)</td>
                <td className="dynamics rise">
                  <span>2.70</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        {/* Группа End */}
        {/* Группа */}
        <div className="group--item">
          {/* Шапка группы */}
          <div className="group--item--heading">
            <div className="left">
              <div className="percent">15.45%</div>
              <div className="sport">
                <IcTableTennis />
                <span className="time">24 с</span>
              </div>
              <div className="forks">
                <button className="btn-copy">
                  <IcStack />
                </button>
                <span>5 вилок</span>
              </div>
            </div>
            <div className="right">
              <button className="btn-calc">
                <IcCalc />
              </button>
              <button className="btn-warn">
                <IcWarning />
              </button>
              <button className="btn-cancel">
                <IcCancel />
              </button>
            </div>
          </div>
          {/* Шапка группы End */}
          {/* Таблица */}
          <table className="group--item--table">
            <tbody>
              <tr>
                <td className="provider">Sportingbull</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">
                    Михаэла Бузэрнеску vs Юлия Гёргес
                  </p>
                  <span className="event-name">(WTA Рим - Грунтовый)</span>
                  <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                </td>
                <td className="bet">П1(8 гейм) (1 сет)</td>
                <td className="dynamics fall">
                  <span>1.35</span>
                </td>
                <td className="close">
                  <button className="btn-close" />
                </td>
              </tr>
              <tr>
                <td className="provider">Vbet</td>
                <td className="event">
                  <button className="copy-btn">
                    <IcCopy />
                  </button>
                  <p className="event-players">
                    Михаэла Бузэрнеску vs Юлия Гёргес
                  </p>
                  <span className="event-name">(WTA Рим - Грунтовый)</span>
                  <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                </td>
                <td className="bet">П2(8 гейм) (1 сет)</td>
                <td className="dynamics rise" />
                <td className="close" />
              </tr>
            </tbody>
          </table>
        </div>
        {/* Группа End */}
      </div>
    );
  }
}

export default LiveForksGroups;
