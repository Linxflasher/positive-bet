import React, { Component } from "react";
import "./live-forks.filter.nav.scss";
import { Link } from "react-router-dom";
import { Label, Icon } from "semantic-ui-react";

import IcGraph from "./IcGraph";
import IcArrTop from "./IcArrTop";
import IcFilter from "./IcFilter";
import IcCalculator from "./IcCalculator";
import IcEye from "./IcEye";

class LiveForksFilterNav extends Component {
  state = {
    updated: false,
    viewSwitched: false,
    calculator: false,
    visible: true
  };

  handleUpdate = this.handleUpdate.bind(this);
  handleViewSwitch = this.handleViewSwitch.bind(this);
  handleCalculator = this.handleCalculator.bind(this);
  handleVisibility = this.handleVisibility.bind(this);

  handleUpdate() {
    this.setState({
      updated: !this.state.updated
    });
  }

  handleViewSwitch() {
    this.setState({
      viewSwitched: !this.state.viewSwitched
    });
  }

  handleCalculator() {
    this.setState({
      calculator: !this.state.calculator
    });
  }

  handleVisibility() {
    this.setState({
      visible: !this.state.visible
    });
  }

  render() {
    return (
      <div className="live-forks--filter-nav">
        <div className="left">
          <button
            className={
              this.state.updated
                ? "btn-round update updated"
                : "btn-round update"
            }
            onClick={this.handleUpdate}
          />
          <button
            className={
              this.state.viewSwitched
                ? "btn-round stack switched"
                : "btn-round stack"
            }
            onClick={this.handleViewSwitch}
          />
          <button
            className={
              this.state.calculator ? "btn-round calc opened" : "btn-round calc"
            }
            onClick={this.handleCalculator}
          >
            <IcCalculator />
          </button>
          <button
            className={
              this.state.visible
                ? "btn-round visibility"
                : "btn-round visibility hidden"
            }
            onClick={this.handleVisibility}
          >
            <IcEye />
          </button>
          <button className="fork-age">
            <IcGraph />
            <span>Возраст вилки</span>
            <IcArrTop />
          </button>
        </div>
        <div className="right">
          <div className="labels">
            <Label className="gray" as="button">
              Футбол Parimatch
              <Icon name="delete" />
            </Label>
            <Label className="brick" as="button">
              Favbet New
              <Icon name="delete" />
            </Label>
            <Label as="button">
              Только Vbet
              <Icon name="delete" />
            </Label>
            <Label as="button">
              Не больше минуты
              <Icon name="delete" />
            </Label>
            <Label as="button">
              Очень длинное название фильтра
              <Icon name="delete" />
            </Label>
            <Label className="gray" as="button">
              Кубок Америки
              <Icon name="delete" />
            </Label>
            <Label as="button">+9</Label>
          </div>
          <Link to="live-forks-filter" className="filters-link">
            <IcFilter />
            <span>Фильтры</span>
          </Link>
        </div>
      </div>
    );
  }
}

export default LiveForksFilterNav;
