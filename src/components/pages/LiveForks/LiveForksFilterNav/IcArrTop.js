import React from "react";

const IcArrTop = () => (
  <svg
    width="9"
    height="6"
    viewBox="0 0 9 6"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8 4.48831L7.48666 5L4.5 2.02289L1.51334 5L1 4.48831L4.4995 1L8 4.48831Z"
      stroke="#333333"
    />
  </svg>
);

export default IcArrTop;
