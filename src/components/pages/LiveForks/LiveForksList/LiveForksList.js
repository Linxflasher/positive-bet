import React, { Component } from "react";
import "./live-forks.list.scss";
import IcTableTennis from "../LiveForksGroup/IcTableTennis";
import IcCalc from "../LiveForksGroup/IcCalc";
import IcWarning from "../LiveForksGroup/IcWarning";
import IcCancel from "../LiveForksGroup/IcCancel";

class LiveForksList extends Component {
  state = {};
  render() {
    return (
      <section className="live-forks--list">
        <table>
          <tbody>
            <tr>
              <td className="td-percent">
                <div className="perc-wrapper">
                  <span className="percent">27.06<span>%</span></span>
                  <span className="sport">
                    <IcTableTennis />
                    <span>1 с</span>
                  </span>
                </div>
              </td>
              <td className="td-btns">
                <div className="btn-group">
                  <button className="btn-calc">
                    <IcCalc />
                  </button>
                  <button className="btn-warn">
                    <IcWarning />
                  </button>
                  <button className="btn-cancel">
                    <IcCancel />
                  </button>
                </div>
              </td>
              <td className="td-provider">
                <div className="provider">Favbet</div>
                <div className="provider">Betcity</div>
              </td>
              <td className="td-event">
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Тимо Болл - Ю Жоу</span>
                  <span className="ev-name">
                    Открытый чемпионат Гонконга
                  </span>
                  <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                </div>
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Boll Timo - Zhou Yu</span>
                  <span className="ev-name">
                    Table Tennis. Men. ITTF World Tour. Hong Kong
                  </span>
                </div>
              </td>
              <td className="td-bet">
                <div className="bet">П1</div>
                <div className="bet">П2</div>
              </td>
              <td className="td-dynamics">
                <div className="fall">2.00</div>
                <br />
                <div className="rise">4.35</div>
              </td>
              <td className="td-close">
                <button className="btn-close" />
              </td>
            </tr>
            <tr>
              <td className="td-percent">
                <div className="perc-wrapper">
                  <span className="percent">27.06<span>%</span></span>
                  <span className="sport">
                    <IcTableTennis />
                    <span>1 с</span>
                  </span>
                </div>
              </td>
              <td className="td-btns">
                <div className="btn-group">
                  <button className="btn-calc">
                    <IcCalc />
                  </button>
                  <button className="btn-warn">
                    <IcWarning />
                  </button>
                  <button className="btn-cancel">
                    <IcCancel />
                  </button>
                </div>
              </td>
              <td className="td-provider">
                <div className="provider">Favbet</div>
                <div className="provider">Betcity</div>
              </td>
              <td className="td-event">
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Тимо Болл - Ю Жоу</span>
                  <span className="ev-name">
                    Открытый чемпионат Гонконга
                  </span>
                  <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                </div>
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Boll Timo - Zhou Yu</span>
                  <span className="ev-name">
                    Table Tennis. Men. ITTF World Tour. Hong Kong
                  </span>
                </div>
              </td>
              <td className="td-bet">
                <div className="bet">П1</div>
                <div className="bet">П2</div>
              </td>
              <td className="td-dynamics">
                <div className="fall">2.00</div>
                <br />
                <div className="rise">4.35</div>
              </td>
              <td className="td-close">
                <button className="btn-close" />
              </td>
            </tr>
            <tr>
              <td className="td-percent">
                <div className="perc-wrapper">
                  <span className="percent">27.06<span>%</span></span>
                  <span className="sport">
                    <IcTableTennis />
                    <span>1 с</span>
                  </span>
                </div>
              </td>
              <td className="td-btns">
                <div className="btn-group">
                  <button className="btn-calc">
                    <IcCalc />
                  </button>
                  <button className="btn-warn">
                    <IcWarning />
                  </button>
                  <button className="btn-cancel">
                    <IcCancel />
                  </button>
                </div>
              </td>
              <td className="td-provider">
                <div className="provider">Favbet</div>
                <div className="provider">Betcity</div>
              </td>
              <td className="td-event">
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Тимо Болл - Ю Жоу</span>
                  <span className="ev-name">
                    Открытый чемпионат Гонконга 1:2
                  </span>
                  <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                </div>
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Boll Timo - Zhou Yu</span>
                  <span className="ev-name">
                    Table Tennis. Men. ITTF World Tour. Hong Kong
                  </span>
                </div>
              </td>
              <td className="td-bet">
                <div className="bet">П1</div>
                <div className="bet">П2</div>
              </td>
              <td className="td-dynamics">
                <div className="fall">2.00</div>
                <br />
                <div className="rise">4.35</div>
              </td>
              <td className="td-close">
                <button className="btn-close" />
              </td>
            </tr>
            <tr>
              <td className="td-percent">
                <div className="perc-wrapper">
                  <span className="percent">27.06<span>%</span></span>
                  <span className="sport">
                    <IcTableTennis />
                    <span>1 с</span>
                  </span>
                </div>
              </td>
              <td className="td-btns">
                <div className="btn-group">
                  <button className="btn-calc">
                    <IcCalc />
                  </button>
                  <button className="btn-warn">
                    <IcWarning />
                  </button>
                  <button className="btn-cancel">
                    <IcCancel />
                  </button>
                </div>
              </td>
              <td className="td-provider">
                <div className="provider">Favbet</div>
                <div className="provider">Betcity</div>
              </td>
              <td className="td-event">
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Тимо Болл - Ю Жоу</span>
                  <span className="ev-name">
                    Открытый чемпионат Гонконга
                  </span>
                  <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                </div>
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Boll Timo - Zhou Yu</span>
                  <span className="ev-name">
                    Table Tennis. Men. ITTF World Tour. Hong Kong
                  </span>
                </div>
              </td>
              <td className="td-bet">
                <div className="bet">П1</div>
                <div className="bet">П2</div>
              </td>
              <td className="td-dynamics">
                <div className="fall">2.00</div>
                <br />
                <div className="rise">4.35</div>
              </td>
              <td className="td-close">
                <button className="btn-close" />
              </td>
            </tr>
            <tr>
              <td className="td-percent">
                <div className="perc-wrapper">
                  <span className="percent">27.06<span>%</span></span>
                  <span className="sport">
                    <IcTableTennis />
                    <span>1 с</span>
                  </span>
                </div>
              </td>
              <td className="td-btns">
                <div className="btn-group">
                  <button className="btn-calc">
                    <IcCalc />
                  </button>
                  <button className="btn-warn">
                    <IcWarning />
                  </button>
                  <button className="btn-cancel">
                    <IcCancel />
                  </button>
                </div>
              </td>
              <td className="td-provider">
                <div className="provider">Favbet</div>
                <div className="provider">Betcity</div>
              </td>
              <td className="td-event">
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Тимо Болл - Ю Жоу</span>
                  <span className="ev-name">
                    Открытый чемпионат Гонконга
                  </span>
                  <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                </div>
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Boll Timo - Zhou Yu</span>
                  <span className="ev-name">
                    Table Tennis. Men. ITTF World Tour. Hong Kong
                  </span>
                </div>
              </td>
              <td className="td-bet">
                <div className="bet">П1</div>
                <div className="bet">П2</div>
              </td>
              <td className="td-dynamics">
                <div className="fall">2.00</div>
                <br />
                <div className="rise">4.35</div>
              </td>
              <td className="td-close">
                <button className="btn-close" />
              </td>
            </tr>
            <tr>
              <td className="td-percent">
                <div className="perc-wrapper">
                  <span className="percent">27.06<span>%</span></span>
                  <span className="sport">
                    <IcTableTennis />
                    <span>1 с</span>
                  </span>
                </div>
              </td>
              <td className="td-btns">
                <div className="btn-group">
                  <button className="btn-calc">
                    <IcCalc />
                  </button>
                  <button className="btn-warn">
                    <IcWarning />
                  </button>
                  <button className="btn-cancel">
                    <IcCancel />
                  </button>
                </div>
              </td>
              <td className="td-provider">
                <div className="provider">Favbet</div>
                <div className="provider">Betcity</div>
              </td>
              <td className="td-event">
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Тимо Болл - Ю Жоу</span>
                  <span className="ev-name">
                    Открытый чемпионат Гонконга
                  </span>
                  <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                </div>
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Boll Timo - Zhou Yu</span>
                  <span className="ev-name">
                    Table Tennis. Men. ITTF World Tour. Hong Kong
                  </span>
                </div>
              </td>
              <td className="td-bet">
                <div className="bet">П1</div>
                <div className="bet">П2</div>
              </td>
              <td className="td-dynamics">
                <div className="fall">2.00</div>
                <br />
                <div className="rise">4.35</div>
              </td>
              <td className="td-close">
                <button className="btn-close" />
              </td>
            </tr>
            <tr>
              <td className="td-percent">
                <div className="perc-wrapper">
                  <span className="percent">27.06<span>%</span></span>
                  <span className="sport">
                    <IcTableTennis />
                    <span>1 с</span>
                  </span>
                </div>
              </td>
              <td className="td-btns">
                <div className="btn-group">
                  <button className="btn-calc">
                    <IcCalc />
                  </button>
                  <button className="btn-warn">
                    <IcWarning />
                  </button>
                  <button className="btn-cancel">
                    <IcCancel />
                  </button>
                </div>
              </td>
              <td className="td-provider">
                <div className="provider">Favbet</div>
                <div className="provider">Betcity</div>
              </td>
              <td className="td-event">
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Тимо Болл - Ю Жоу</span>
                  <span className="ev-name">
                    Открытый чемпионат Гонконга
                  </span>
                  <span className="ev-score">1:2 (9:11,9:11,11:9,1:0)</span>
                </div>
                <div className="event">
                  <button className="ev-copy" />
                  <span className="ev-part">Boll Timo - Zhou Yu</span>
                  <span className="ev-name">
                    Table Tennis. Men. ITTF World Tour. Hong Kong
                  </span>
                </div>
              </td>
              <td className="td-bet">
                <div className="bet">П1</div>
                <div className="bet">П2</div>
              </td>
              <td className="td-dynamics">
                <div className="fall">2.00</div>
                <br />
                <div className="rise">4.35</div>
              </td>
              <td className="td-close">
                <button className="btn-close" />
              </td>
            </tr>
          </tbody>
        </table>
      </section>
    );
  }
}

export default LiveForksList;
