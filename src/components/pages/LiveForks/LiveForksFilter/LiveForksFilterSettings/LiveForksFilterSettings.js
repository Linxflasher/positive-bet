import React, { Component } from "react";
import { Accordion } from "semantic-ui-react";
import "./live-forks.filter.settings.scss";
import Settings from "./Settings/Settings";
import BookmakerOffices from "./BookmakerOffices/BookmakerOffices";


class LiveForksFilterSettings extends Component {
  state = { activeIndex: 0 };

  handleClick = this.handleClick.bind(this);

  handleClick(e, titleProps) {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({ activeIndex: newIndex });
  }

  render() {
    return (
      <Accordion exclusive={false}>
        <Accordion.Accordion className="it-1">
          <Accordion.Title
            active={this.state.activeIndex === 0}
            index={0}
            onClick={this.handleClick}
          >
            <div className="acc--item--title">
              <h2>Настройки фильтра</h2>
              <span>
                {this.state.activeIndex === 0 ? "Свернуть" : "Развернуть"}
              </span>
            </div>
          </Accordion.Title>
          <Accordion.Content active={this.state.activeIndex === 0}>
            <div className="acc--item--content settings">
              <Settings/>
            </div>
          </Accordion.Content>
        </Accordion.Accordion>
        <Accordion.Accordion className="it-2">
          <Accordion.Title
            active={this.state.activeIndex === 1}
            index={1}
            onClick={this.handleClick}
          >
            <div className="acc--item--title">
              <h2>Букмекерские конторы</h2>
              <span>
                {this.state.activeIndex === 1 ? "Свернуть" : "Развернуть"}
              </span>
            </div>
          </Accordion.Title>
          <Accordion.Content active={this.state.activeIndex === 1}>
            <div className="acc--item--content bookmakers">
              <BookmakerOffices/>
            </div>
          </Accordion.Content>
        </Accordion.Accordion>
        <Accordion.Accordion className="it-3">
          <Accordion.Title
            active={this.state.activeIndex === 2}
            index={2}
            onClick={this.handleClick}
          >
            <div className="acc--item--title">
              <h2>Правила</h2>
              <span>
                {this.state.activeIndex === 2 ? "Свернуть" : "Развернуть"}
              </span>
            </div>
          </Accordion.Title>
          <Accordion.Content active={this.state.activeIndex === 2}>
            <div className="acc--item--content">
              <p>
                Контент
              </p>
            </div>
          </Accordion.Content>
        </Accordion.Accordion>
      </Accordion>
    );
  }
}

export default LiveForksFilterSettings;
