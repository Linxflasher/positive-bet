import React, { Component } from "react";
import "./bookmaker-offices.scss";
import IcClose from "./ic-close-blue.svg";
import IcFootball from "../Settings/BetsSettings/ic-football.svg";
import IcTennis from "../Settings/BetsSettings/ic-tennis.svg";
import IcHockey from "../Settings/BetsSettings/ic-hockey.svg";
import IcBasketball from "../Settings/BetsSettings/ic-basketball.svg";
import IcVolleyball from "../Settings/BetsSettings/ic-volleyball.svg";
import IcHandball from "../Settings/BetsSettings/ic-handball.svg";
import IcTableTennis from "../Settings/BetsSettings/ic-table-tennis.svg";
import IcFutsal from "../Settings/BetsSettings/ic-futsal.svg";
import IcBadminton from "../Settings/BetsSettings/ic-badminton.svg";
import IcBaseball from "../Settings/BetsSettings/ic-baseball.svg";

class Bookmakers extends Component {
  state = {};
  render() {
    return (
      <div className="bookmaker-offices">
        <div className="bookmaker-offices--item">
          <div className="table">
            <div className="tr">
              <div className="th toggler">
                <span>О</span>
                <span>Н</span>
                <span>Отл</span>
              </div>
              <div className="th bk">БК</div>
              <div className="th bet-types">Типы ставок</div>
              <div className="th rate-from">Коэфф. от</div>
              <div className="th rate-to">до</div>
              <div className="th initiator">Инициатор</div>
              <div className="th clones">Клоны</div>
            </div>
          </div>
        </div>

        <div className="bookmaker-offices--item">
          <div className="table">
            <div className="tr">
              <div className="td toggler">
                <div className="options-toggler not-obligatory">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td bk">10bet</div>
              <div className="td bet-types">
                <div className="form-row radio">
                  <input type="radio" id="bet-1" name="bet" />
                  <label htmlFor="bet-1"></label>
                </div>
                <button className="more-btn"></button>
              </div>
              <div className="td rate-from">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td rate-to">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td initiator">
                <div className="options-toggler">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td clones">
                <strong>10bet</strong>, 377bet, Adjarabet, Etoto, NetBet, Rubet,
                ComeOn, JenningsBet, Mobilbet
              </div>
            </div>
          </div>
          <div className="bookmaker-offices--item--params">
            <button className="close-btn">
              <span>Закрыть</span>
              <img src={IcClose} alt=""/>
            </button>
            <h2>Дополнительные параметры БК 188bet</h2>
            <div className="enable-bet-types">
              <button className="switcher active"></button>
              <span>использовать типы ставок для букмекера</span>
            </div>
            <div className="bets-settings-wrapper">
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-2" name="football-2" />
                            <label htmlFor="football-2">футбол</label>
                            <img src={IcFootball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-wins-2" name="football-wins-2" />
                            <label htmlFor="football-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-totals-2" name="football-totals-2" />
                            <label htmlFor="football-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-totals-ind-2" name="football-totals-ind-2" />
                            <label htmlFor="football-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-odds-2" name="football-odds-2" />
                            <label htmlFor="football-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-corners-2" name="football-corners-2" />
                            <label htmlFor="football-corners-2">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-2" name="tennis-2" />
                            <label htmlFor="tennis-2">теннис</label>
                            <img src={IcTennis} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-wins-2" name="tennis-wins-2" />
                            <label htmlFor="tennis-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-totals-2" name="tennis-totals-2" />
                            <label htmlFor="tennis-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-totals-ind-2" name="tennis-totals-ind-2" />
                            <label htmlFor="tennis-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-odds-2" name="tennis-odds-2" />
                            <label htmlFor="tennis-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-game-2" name="tennis-game-2" />
                            <label htmlFor="tennis-game-2">поб. в гейме</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-set-2" name="tennis-set-2" />
                            <label htmlFor="tennis-set-2">победы в сете</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-game-cur-2" name="tennis-game-cur-2" />
                            <label htmlFor="tennis-game-cur-2">победы в текущем гейме</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-2" name="hockey-2" />
                            <label htmlFor="hockey-2">хоккей</label>
                            <img src={IcHockey} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-wins-2" name="hockey-wins-2" />
                            <label htmlFor="hockey-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-totals-2" name="hockey-totals-2" />
                            <label htmlFor="hockey-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-totals-ind-2" name="hockey-totals-ind-2" />
                            <label htmlFor="hockey-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-odds-2" name="hockey-odds-2" />
                            <label htmlFor="hockey-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-corners-2" name="hockey-corners-2" />
                            <label htmlFor="hockey-corners-2">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-2" name="basketball-2" />
                            <label htmlFor="basketball-2">баскетбол</label>
                            <img src={IcBasketball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-wins-2" name="basketball-wins-2" />
                            <label htmlFor="basketball-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-totals-2" name="basketball-totals-2" />
                            <label htmlFor="basketball-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-totals-ind-2" name="basketball-totals-ind-2" />
                            <label htmlFor="basketball-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-odds-2" name="basketball-odds-2" />
                            <label htmlFor="basketball-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-corners-2" name="basketball-corners-2" />
                            <label htmlFor="basketball-corners-2">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-2" name="volleyball-2" />
                            <label htmlFor="volleyball-2">волейбол</label>
                            <img src={IcVolleyball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-wins-2" name="volleyball-wins-2" />
                            <label htmlFor="volleyball-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-totals-2" name="volleyball-totals-2" />
                            <label htmlFor="volleyball-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-totals-ind-2" name="volleyball-totals-ind-2" />
                            <label htmlFor="volleyball-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-odds-2" name="volleyball-odds-2" />
                            <label htmlFor="volleyball-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-corners-2" name="volleyball-corners-2" />
                            <label htmlFor="volleyball-corners-2">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-2" name="handball-2" />
                            <label htmlFor="handball-2">гандбол</label>
                            <img src={IcHandball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-wins-2" name="handball-wins-2" />
                            <label htmlFor="handball-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-totals-2" name="handball-totals-2" />
                            <label htmlFor="handball-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-totals-ind-2" name="handball-totals-ind-2" />
                            <label htmlFor="handball-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-odds-2" name="handball-odds-2" />
                            <label htmlFor="handball-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-corners-2" name="handball-corners-2" />
                            <label htmlFor="handball-corners-2">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-2" name="table-tennis-2" />
                            <label htmlFor="table-tennis-2">наст. теннис</label>
                            <img src={IcTableTennis} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-wins-2" name="table-tennis-wins-2" />
                            <label htmlFor="table-tennis-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-totals-2" name="table-tennis-totals-2" />
                            <label htmlFor="table-tennis-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-totals-ind-2" name="table-tennis-totals-ind-2" />
                            <label htmlFor="table-tennis-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-odds-2" name="table-tennis-odds-2" />
                            <label htmlFor="table-tennis-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-corners-2" name="table-tennis-corners-2" />
                            <label htmlFor="table-tennis-corners-2">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-2" name="futsal-2" />
                            <label htmlFor="futsal-2">футзал</label>
                            <img src={IcFutsal} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-wins-2" name="futsal-wins-2" />
                            <label htmlFor="futsal-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-totals-2" name="futsal-totals-2" />
                            <label htmlFor="futsal-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-totals-ind-2" name="futsal-totals-ind-2" />
                            <label htmlFor="futsal-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-odds-2" name="futsal-odds-2" />
                            <label htmlFor="futsal-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-corners-2" name="futsal-corners-2" />
                            <label htmlFor="futsal-corners-2">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-2" name="badminton-2" />
                            <label htmlFor="badminton-2">бадминтон</label>
                            <img src={IcBadminton} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-wins-2" name="badminton-wins-2" />
                            <label htmlFor="badminton-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-totals-2" name="badminton-totals-2" />
                            <label htmlFor="badminton-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-totals-ind-2" name="badminton-totals-ind-2" />
                            <label htmlFor="badminton-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-odds-2" name="badminton-odds-2" />
                            <label htmlFor="badminton-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-corners-2" name="badminton-corners-2" />
                            <label htmlFor="badminton-corners-2">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-2" name="baseball-2" />
                            <label htmlFor="baseball-2">бейсбол</label>
                            <img src={IcBaseball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-wins-2" name="baseball-wins-2" />
                            <label htmlFor="baseball-wins-2">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-totals-2" name="baseball-totals-2" />
                            <label htmlFor="baseball-totals-2">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-totals-ind-2" name="baseball-totals-ind-2" />
                            <label htmlFor="baseball-totals-ind-2">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-odds-2" name="baseball-odds-2" />
                            <label htmlFor="baseball-odds-2">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-corners-2" name="baseball-corners-2" />
                            <label htmlFor="baseball-corners-2">угловые</label>
                        </div>
                    </div>
                </div>
          </div>
        </div>

        <div className="bookmaker-offices--item">
          <div className="table">
            <div className="tr">
              <div className="td toggler">
                <div className="options-toggler obligatory">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td bk">188bet</div>
              <div className="td bet-types">
                <div className="form-row radio">
                  <input type="radio" id="bet-2" name="bet" />
                  <label htmlFor="bet-2"></label>
                </div>
                <button className="more-btn"></button>
              </div>
              <div className="td rate-from">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td rate-to">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td initiator">
                <div className="options-toggler">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td clones">188bet, RoadBet</div>
            </div>
          </div>
        </div>

        <div className="bookmaker-offices--item">
          <div className="table">
            <div className="tr">
              <div className="td toggler">
                <div className="options-toggler obligatory">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td bk">1win</div>
              <div className="td bet-types">
                <div className="form-row radio">
                  <input type="radio" id="bet-3" name="bet" />
                  <label htmlFor="bet-3"></label>
                </div>
                <button className="more-btn"></button>
              </div>
              <div className="td rate-from">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td rate-to">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td initiator">
                <div className="options-toggler">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td clones">
                <span>&mdash;</span>
              </div>
            </div>
          </div>
        </div>

        <div className="bookmaker-offices--item">
          <div className="table">
            <div className="tr">
              <div className="td toggler">
                <div className="options-toggler not-obligatory">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td bk">1xbet</div>
              <div className="td bet-types">
                <div className="form-row radio">
                  <input type="radio" id="bet-4" name="bet" />
                  <label htmlFor="bet-4"></label>
                </div>
                <button className="more-btn"></button>
              </div>
              <div className="td rate-from">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td rate-to">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td initiator">
                <div className="options-toggler">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td clones">
                <strong>1xbet</strong>, Melbet, 1xbit, 1xbit, 1xstavka
              </div>
            </div>
          </div>
        </div>

        <div className="bookmaker-offices--item">
          <div className="table">
            <div className="tr">
              <div className="td toggler">
                <div className="options-toggler not-obligatory">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td bk">Baltbet</div>
              <div className="td bet-types">
                <div className="form-row radio">
                  <input type="radio" id="bet-5" name="bet" />
                  <label htmlFor="bet-5"></label>
                </div>
                <button className="more-btn"></button>
              </div>
              <div className="td rate-from">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td rate-to">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td initiator">
                <div className="options-toggler">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td clones">
                <strong>Baltbet</strong>, Baltbet.ru
              </div>
            </div>
          </div>
        </div>

        <div className="bookmaker-offices--item">
          <div className="table">
            <div className="tr">
              <div className="td toggler">
                <div className="options-toggler different">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td bk">Bet-At-Home</div>
              <div className="td bet-types">
                <div className="form-row radio">
                  <input type="radio" id="bet-6" name="bet" />
                  <label htmlFor="bet-6"></label>
                </div>
                <button className="more-btn"></button>
              </div>
              <div className="td rate-from">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td rate-to">
                <div className="form-row">
                  <input type="number" placeholder="0.00" />
                </div>
              </div>
              <div className="td initiator">
                <div className="options-toggler">
                  <button className="obligatory"></button>
                  <button className="not-obligatory"></button>
                  <button className="different"></button>
                </div>
              </div>
              <div className="td clones">
                <span>&mdash;</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Bookmakers;
