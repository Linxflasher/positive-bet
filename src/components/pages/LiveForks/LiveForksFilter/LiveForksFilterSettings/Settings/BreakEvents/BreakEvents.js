import React, { Component } from "react";
import "./break-events.scss";

import IcFootball from "./ic-football-blue.svg";
import IcTennis from "./ic-tennis-blue.svg";
import IcHockey from "./ic-hockey-blue.svg";
import IcBasketball from "./ic-basketball-blue.svg";
import IcVolleyball from "./ic-volleyball-blue.svg";
import IcHandball from "./ic-handball-blue.svg";
import IcTableTennis from "./ic-table-tennis-blue.svg";
import IcFutsal from "./ic-futsal-blue.svg";
import IcBadminton from "./ic-badminton-blue.svg";
import IcBaseball from "./ic-baseball-blue.svg";

class BreakEvents extends Component {
  state = {
    breakEvent: "obligatory"
  };

  handleEventObligatory = this.handleEventObligatory.bind(this);
  handleEventNotObligatory = this.handleEventNotObligatory.bind(this);
  handleEventDifferent = this.handleEventDifferent.bind(this);

  handleEventObligatory() {
    this.setState({
      breakEvent: "obligatory"
    })
  }

  handleEventNotObligatory() {
    this.setState({
      breakEvent: "not-obligatory"
    })
  }

  handleEventDifferent() {
    this.setState({
      breakEvent: "different"
    })
  }

  render() {
    return (
      <div className="break-events">
        <h3>События в перерыве</h3>
        <ul className="break-events--list">
          <li>
            <div className={this.state.breakEvent === "obligatory" ? "options-toggler obligatory" : this.state.breakEvent === "not-obligatory" ? "options-toggler not-obligatory" : this.state.breakEvent === "different" ? "options-toggler different" : "options-toggler"}>
              <button className="obligatory" onClick={this.handleEventObligatory}></button>
              <button className="not-obligatory" onClick={this.handleEventNotObligatory}></button>
              <button className="different" onClick={this.handleEventDifferent}></button>
            </div>
            <div className="sport">
              <img src={IcFootball} alt="" />
              <span className="name">футбол</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcTennis} alt="" />
              <span className="name">теннис</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcHockey} alt="" />
              <span className="name">хоккей</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcBasketball} alt="" />
              <span className="name">баскетболл</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcVolleyball} alt="" />
              <span className="name">волейбол</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcHandball} alt="" />
              <span className="name">гандбол</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcTableTennis} alt="" />
              <span className="name">настольный теннис</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcFutsal} alt="" />
              <span className="name">футзал</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcBadminton} alt="" />
              <span className="name">бадминтон</span>
            </div>
          </li>
          <li>
            <div className="options-toggler">
              <button className="obligatory"></button>
              <button className="not-obligatory"></button>
              <button className="different"></button>
            </div>
            <div className="sport">
              <img src={IcBaseball} alt="" />
              <span className="name">бейсбол</span>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}

export default BreakEvents;
