import React from "react";

import ForkParams from "./ForkParams/ForkParams";
import BetsSettings from "./BetsSettings/BetsSettings";
import BreakEvents from "./BreakEvents/BreakEvents";

const Settings = () => (
  <React.Fragment>
    <div className="left">
      <ForkParams />
      <BreakEvents />
    </div>
    <div className="right">
      <BetsSettings />
    </div>
  </React.Fragment>
);

export default Settings;
