import React, { Component } from 'react';
import "./fork-params.scss";

class ForkParams extends Component {
    state = {  }
    render() {
        return (
            <div className="fork-params">
                <h3>Параметры вилки</h3>
                <form>
                    <div className="fork-params--item">
                        <div className="form-row">
                            <label htmlFor="income-from">Доходы от</label>
                            <input type="number" id="income-from" name="income-from" placeholder="0.00" />
                        </div>
                        <div className="form-row">
                            <label htmlFor="income-to">до</label>
                            <input type="number" id="income-to" name="income-to" placeholder="0.00" />
                        </div>
                        <span>%</span>
                    </div>
                    <div className="fork-params--item">
                        <div className="form-row">
                            <label htmlFor="rate-from">Коэфф. от</label>
                            <input type="number" id="rate-from" name="rate-from" placeholder="0.00" />
                        </div>
                        <div className="form-row">
                            <label htmlFor="rate-to">до</label>
                            <input type="number" id="rate-to" name="rate-to" placeholder="0.00" />
                        </div>
                    </div>
                    <div className="fork-params--item">
                        <div className="form-row">
                            <label htmlFor="age-from">Возраст от</label>
                            <input type="number" id="age-from" name="age-from" placeholder="0.00" />
                        </div>
                        <div className="form-row">
                            <label htmlFor="age-to">до</label>
                            <input type="number" id="age-to" name="age-to" placeholder="0.00" />
                        </div>
                        <span>с</span>
                    </div>
                    <div className="fork-params--item">
                        <div className="form-row">
                            <label htmlFor="number-from">Число от</label>
                            <input type="number" id="number-from" name="number-from" placeholder="0.00" />
                        </div>
                        <div className="form-row">
                            <label htmlFor="number-to">до</label>
                            <input type="number" id="number-to" name="number-to" placeholder="0.00" />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default ForkParams;