import React, { Component } from 'react';
import "./bets-settings.scss";
import IcFootball from "./ic-football.svg";
import IcTennis from "./ic-tennis.svg";
import IcHockey from "./ic-hockey.svg";
import IcBasketball from "./ic-basketball.svg";
import IcVolleyball from "./ic-volleyball.svg";
import IcHandball from "./ic-handball.svg";
import IcTableTennis from "./ic-table-tennis.svg";
import IcFutsal from "./ic-futsal.svg";
import IcBadminton from "./ic-badminton.svg";
import IcBaseball from "./ic-baseball.svg";

class BetsSettings extends Component {
    state = {  }
    render() {
        return (
            <div className="bets-settings">
                <h3>Типы исходов</h3>
                <div className="bets-settings-wrapper">
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football" name="football" />
                            <label htmlFor="football">футбол</label>
                            <img src={IcFootball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-wins" name="football-wins" />
                            <label htmlFor="football-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-totals" name="football-totals" />
                            <label htmlFor="football-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-totals-ind" name="football-totals-ind" />
                            <label htmlFor="football-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-odds" name="football-odds" />
                            <label htmlFor="football-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="football-corners" name="football-corners" />
                            <label htmlFor="football-corners">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis" name="tennis" />
                            <label htmlFor="tennis">теннис</label>
                            <img src={IcTennis} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-wins" name="tennis-wins" />
                            <label htmlFor="tennis-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-totals" name="tennis-totals" />
                            <label htmlFor="tennis-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-totals-ind" name="tennis-totals-ind" />
                            <label htmlFor="tennis-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-odds" name="tennis-odds" />
                            <label htmlFor="tennis-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-game" name="tennis-game" />
                            <label htmlFor="tennis-game">поб. в гейме</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-set" name="tennis-set" />
                            <label htmlFor="tennis-set">победы в сете</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="tennis-game-cur" name="tennis-game-cur" />
                            <label htmlFor="tennis-game-cur">победы в текущем гейме</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey" name="hockey" />
                            <label htmlFor="hockey">хоккей</label>
                            <img src={IcHockey} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-wins" name="hockey-wins" />
                            <label htmlFor="hockey-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-totals" name="hockey-totals" />
                            <label htmlFor="hockey-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-totals-ind" name="hockey-totals-ind" />
                            <label htmlFor="hockey-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-odds" name="hockey-odds" />
                            <label htmlFor="hockey-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="hockey-corners" name="hockey-corners" />
                            <label htmlFor="hockey-corners">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball" name="basketball" />
                            <label htmlFor="basketball">баскетбол</label>
                            <img src={IcBasketball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-wins" name="basketball-wins" />
                            <label htmlFor="basketball-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-totals" name="basketball-totals" />
                            <label htmlFor="basketball-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-totals-ind" name="basketball-totals-ind" />
                            <label htmlFor="basketball-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-odds" name="basketball-odds" />
                            <label htmlFor="basketball-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="basketball-corners" name="basketball-corners" />
                            <label htmlFor="basketball-corners">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball" name="volleyball" />
                            <label htmlFor="volleyball">волейбол</label>
                            <img src={IcVolleyball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-wins" name="volleyball-wins" />
                            <label htmlFor="volleyball-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-totals" name="volleyball-totals" />
                            <label htmlFor="volleyball-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-totals-ind" name="volleyball-totals-ind" />
                            <label htmlFor="volleyball-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-odds" name="volleyball-odds" />
                            <label htmlFor="volleyball-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="volleyball-corners" name="volleyball-corners" />
                            <label htmlFor="volleyball-corners">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball" name="handball" />
                            <label htmlFor="handball">гандбол</label>
                            <img src={IcHandball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-wins" name="handball-wins" />
                            <label htmlFor="handball-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-totals" name="handball-totals" />
                            <label htmlFor="handball-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-totals-ind" name="handball-totals-ind" />
                            <label htmlFor="handball-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-odds" name="handball-odds" />
                            <label htmlFor="handball-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="handball-corners" name="handball-corners" />
                            <label htmlFor="handball-corners">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis" name="table-tennis" />
                            <label htmlFor="table-tennis">наст. теннис</label>
                            <img src={IcTableTennis} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-wins" name="table-tennis-wins" />
                            <label htmlFor="table-tennis-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-totals" name="table-tennis-totals" />
                            <label htmlFor="table-tennis-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-totals-ind" name="table-tennis-totals-ind" />
                            <label htmlFor="table-tennis-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-odds" name="table-tennis-odds" />
                            <label htmlFor="table-tennis-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="table-tennis-corners" name="table-tennis-corners" />
                            <label htmlFor="table-tennis-corners">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal" name="futsal" />
                            <label htmlFor="futsal">футзал</label>
                            <img src={IcFutsal} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-wins" name="futsal-wins" />
                            <label htmlFor="futsal-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-totals" name="futsal-totals" />
                            <label htmlFor="futsal-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-totals-ind" name="futsal-totals-ind" />
                            <label htmlFor="futsal-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-odds" name="futsal-odds" />
                            <label htmlFor="futsal-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="futsal-corners" name="futsal-corners" />
                            <label htmlFor="futsal-corners">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton" name="badminton" />
                            <label htmlFor="badminton">бадминтон</label>
                            <img src={IcBadminton} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-wins" name="badminton-wins" />
                            <label htmlFor="badminton-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-totals" name="badminton-totals" />
                            <label htmlFor="badminton-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-totals-ind" name="badminton-totals-ind" />
                            <label htmlFor="badminton-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-odds" name="badminton-odds" />
                            <label htmlFor="badminton-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="badminton-corners" name="badminton-corners" />
                            <label htmlFor="badminton-corners">угловые</label>
                        </div>
                    </div>
                    <div className="bets-settings--item">
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball" name="baseball" />
                            <label htmlFor="baseball">бейсбол</label>
                            <img src={IcBaseball} alt="" />
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-wins" name="baseball-wins" />
                            <label htmlFor="baseball-wins">подебы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-totals" name="baseball-totals" />
                            <label htmlFor="baseball-totals">тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-totals-ind" name="baseball-totals-ind" />
                            <label htmlFor="baseball-totals-ind">инд. тоталы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-odds" name="baseball-odds" />
                            <label htmlFor="baseball-odds">форы</label>
                        </div>
                        <div className="form-row checkbox">
                            <input type="checkbox" id="baseball-corners" name="baseball-corners" />
                            <label htmlFor="baseball-corners">угловые</label>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BetsSettings;