import React, { Component } from "react";
import "./live-forks.filter.scss";
import { Dropdown } from "semantic-ui-react";
import Header from "../../../template/Header/Header";
import Footer from "../../../template/Footer/Footer";

import LiveForksFilterSettings from "./LiveForksFilterSettings/LiveForksFilterSettings";

const colorsList = [
  {
    key: "1",
    text: "Зелёный",
    value: "1",
    label: { color: 'green', empty: true, circular: true },
  },
  {
    key: "2",
    text: "Красный",
    value: "2",
    label: { color: 'red', empty: true, circular: true },
  },
  {
    key: "3",
    text: "Синий",
    value: "3",
    label: { color: 'blue', empty: true, circular: true },
  }
];

const soundsList = [
  {
    key: "1",
    text: "Эмбиент номер один",
    value: "1"
  },
  {
    key: "2",
    text: "Эмбиент номер два",
    value: "2"
  },
  {
    key: "3",
    text: "Сирена номер один",
    value: "3"
  },
  {
    key: "4",
    text: "Сирена номер два",
    value: "4"
  },
  {
    key: "5",
    text: "Громкий бас",
    value: "5"
  },
  {
    key: "6",
    text: "Звонкий бас",
    value: "6"
  },
  {
    key: "7",
    text: "Тихий звонок",
    value: "7"
  },
  {
    key: "8",
    text: "Классика номер один",
    value: "8"
  },
  {
    key: "9",
    text: "Классика номер два",
    value: "9"
  }
];

class LiveForksFilter extends Component {
  state = {};
  render() {
    return (
      <div className="page-wrapper">
        <Header />
        <div className="live-forks--filter">

          <div className="live-forks--filter--top">
            <div className="content">
              <div className="title">
                <h1>Новый фильтр</h1>
                <button className="btn-close"></button>
              </div>
              <div className="filter">
                <div className="form-row filter--name">
                  <label htmlFor="filter-name">Имя фильтра</label>
                  <input type="text" id="filter-name" name="filter-name" />
                </div>
                <div className="form-row filter--color">
                  <label htmlFor="">Цвет фильтра</label>
                  <Dropdown
                    className="color-dropdown"
                    defaultValue="1"
                    placeholder=""
                    selection
                    options={colorsList}
                  />
                </div>
                <div className="form-row filter--sound">
                  <label htmlFor="">Звук фильтра</label>
                  <Dropdown
                    className="sound-dropdown"
                    defaultValue="1"
                    placeholder=""
                    selection
                    options={soundsList}
                  />
                </div>
                <button className="btn btn-save">Сохранить</button>
              </div>
            </div>
          </div>

          <div className="live-forks--filter--settings">
            <LiveForksFilterSettings/>
          </div>

        </div>
        <Footer />
      </div>
    );
  }
}

export default LiveForksFilter;
