import React, { Component } from "react";
import "./live-forks.group.extended.scss";

import IcCalc from "../LiveForksGroup/IcCalc";
import IcWarning from "../LiveForksGroup/IcWarning";
import IcCancel from "../LiveForksGroup/IcCancel";
import IcCopy from "../LiveForksGroup/IcCopy";
import IcTableTennis from "../LiveForksGroup/IcTableTennis";
import IcClear from "./ic-clear.svg";

import LiveForksCalculator from "../LiveForksCalculator/LiveForksCalculator";

class LiveForksGroupExtended extends Component {
  state = {};
  render() {
    return (
      <div className="live-forks--group forks">
        <form>
          <LiveForksCalculator />
          <div className="group--item--title">
            <h2>
              Тимо Болл - Ю Жоу<span>Открытый чемпионат Гонконга</span>
            </h2>
            <button className="clear-btn">
              <span>Очистить</span>
              <img src={IcClear} alt="" />
            </button>
          </div>
          <div className="group--item--filters">
            <div className="form-row checkbox">
              <input type="checkbox" id="wins" name="wins" />
              <label htmlFor="wins">Победы</label>
            </div>
            <div className="form-row checkbox">
              <input type="checkbox" id="total" name="total" />
              <label htmlFor="total">Тоталы</label>
            </div>
            <div className="form-row checkbox">
              <input type="checkbox" id="unique-total" name="unique-total" />
              <label htmlFor="unique-total">Инд. Тоталы</label>
            </div>
            <div className="form-row checkbox">
              <input type="checkbox" id="odds" name="odds" />
              <label htmlFor="odds">Форы</label>
            </div>
            <div className="form-row checkbox">
              <input type="checkbox" id="wins-game" name="wins-game" />
              <label htmlFor="wins-game">Поб. в гейме</label>
            </div>
            <div className="form-row checkbox">
              <input type="checkbox" id="wins-set" name="wins-set" />
              <label htmlFor="wins-set">Поб. в сете</label>
            </div>
            <div className="form-row checkbox">
              <input type="checkbox" id="wins-current" name="wins-current" />
              <label htmlFor="wins-current">Поб. в тек. гейме</label>
            </div>
          </div>
          {/* Группа */}
          <div className="group--item">
            {/* Шапка группы */}
            <div className="group--item--heading">
              <div className="left">
                <div className="percent">27.06%</div>
                <div className="sport">
                  <IcTableTennis />
                  <span className="time">2 с</span>
                </div>
              </div>
              <div className="right">
                <button className="btn-calc active">
                  <IcCalc />
                </button>
                <button className="btn-warn">
                  <IcWarning />
                </button>
                <button className="btn-cancel">
                  <IcCancel />
                </button>
              </div>
            </div>
            {/* Шапка группы End */}
            {/* Таблица */}
            <table className="group--item--table">
              <tbody>
                <tr>
                  <td className="provider">Favbet</td>
                  <td className="event">
                    <button className="copy-btn">
                      <IcCopy />
                    </button>
                    <p className="event-players">Тимо Болл - Ю Жоу</p>
                    <span className="event-name">
                      Открытый чемпионат Гонконга 1:2
                    </span>
                    <span className="event-score">(9:11,9:11,11:9,1:0)</span>
                  </td>
                  <td className="bet">П1</td>
                  <td className="dynamics fall">
                    <span>2.00</span>
                  </td>
                  <td className="close">
                    <button className="btn-close" />
                  </td>
                </tr>
                <tr>
                  <td className="provider">Betcity</td>
                  <td className="event">
                    <button className="copy-btn">
                      <IcCopy />
                    </button>
                    <p className="event-players">Boll Timo - Zhou Yu</p>
                    <span className="event-name">
                      Table Tennis. Men. ITTF World Tour. Hong Kong{" "}
                    </span>
                  </td>
                  <td className="bet">П2</td>
                  <td className="dynamics rise">
                    <span>4.35</span>
                  </td>
                  <td className="close">
                    <button className="btn-close" />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          {/* Группа End */}
          {/* Группа */}
          <div className="group--item">
            {/* Шапка группы */}
            <div className="group--item--heading">
              <div className="left">
                <div className="percent">22.21%</div>
                <div className="sport">
                  <IcTableTennis />
                  <span className="time">4 с</span>
                </div>
              </div>
              <div className="right">
                <button className="btn-calc">
                  <IcCalc />
                </button>
              </div>
            </div>
            {/* Шапка группы End */}
            {/* Таблица */}
            <table className="group--item--table">
              <tbody>
                <tr>
                  <td className="provider">LigaStavok</td>
                  <td className="event">
                    <button className="copy-btn">
                      <IcCopy />
                    </button>
                    <p className="event-players">Болл Т. - Чжоу Юй</p>
                    <span className="event-name">
                      (Теннис. WTA. Рим. Грунт)
                    </span>
                    <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                  </td>
                  <td className="bet">П1(8 гейм) (1 сет)</td>
                  <td className="dynamics fall">
                    <span>2.65</span>
                  </td>
                  <td className="close">
                    <button className="btn-close" />
                  </td>
                </tr>
                <tr>
                  <td className="provider">Betcity</td>
                  <td className="event">
                    <button className="copy-btn">
                      <IcCopy />
                    </button>
                    <p className="event-players">
                      Михаэла Бузэрнеску vs Юлия Гёргес
                    </p>
                    <span className="event-name">WTA Рим - Грунтовый</span>
                    <span className="event-score">1:0 (6:4, 2:5), 2 сет</span>
                  </td>
                  <td className="bet">П2(8 гейм) (1 сет)</td>
                  <td className="dynamics rise">
                    <span>2.70</span>
                  </td>
                  <td className="close">
                    <button className="btn-close" />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          {/* Группа End */}
        </form>
      </div>
    );
  }
}

export default LiveForksGroupExtended;
