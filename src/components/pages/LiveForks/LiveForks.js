import React, { Component } from "react";
import "./live-forks.scss";

import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

import LiveForksFilterNav from "./LiveForksFilterNav/LiveForksFilterNav";
import LiveForksGroup from "./LiveForksGroup/LiveForksGroup";
import LiveForksGroupExtended from "./LiveForksGroupExtended/LiveForksGroupExtended";
import LiveForksCalculatorWide from "./LiveForksCalculatorWide/LiveForksCalculatorWide";
import LiveForksList from "./LiveForksList/LiveForksList";
import LiveForksSideFilter from "./LiveForksSideFilter/LiveForksSideFilter";
import LiveForksSideHidden from "./LiveForksSideHidden/LiveForksSideHidden";

class LiveForks extends Component {
  state = {};
  render() {
    return (
      <div className="page-wrapper">
        <Header />
        <div className="live-forks">
          <LiveForksFilterNav />
          <LiveForksCalculatorWide />
          <LiveForksList/>
          <section className="live-forks--content">
            <LiveForksGroup />
            <LiveForksGroupExtended />
            <LiveForksSideHidden/>
            <LiveForksSideFilter/>
          </section>
        </div>

        <Footer />
      </div>
    );
  }
}

export default LiveForks;
