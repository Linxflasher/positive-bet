import React, { Component } from "react";
import { Label } from "semantic-ui-react";
import "./live-forks.side.hidden.scss";

class LiveForksSideHidden extends Component {
  state = {};
  render() {
    return (
      <div className="live-forks--side--hidden">
        <form>
          <h4>Показать скрытые</h4>
          <ul className="list">
            <li>
              <div className="title">
                <span>Вилки</span>
                <Label>56</Label>
              </div>
              <button className="btn-delete"></button>
            </li>
            <li>
              <div className="title">
                <span>События</span>
                <Label>134</Label>
              </div>
              <button className="btn-delete"></button>
            </li>
            <li>
              <div className="title">
                <span>События по БК</span>
                <Label>45</Label>
              </div>
              <button className="btn-delete"></button>
            </li>
            <li>
              <div className="title">
                <span>Исходы</span>
                <Label>12</Label>
              </div>
              <button className="btn-delete"></button>
            </li>
          </ul>
        </form>
      </div>
    );
  }
}

export default LiveForksSideHidden;
