import React, { Component } from "react";
import "./profile.bookmakers.scss";

import { Dropdown } from "semantic-ui-react";
import Alert from "../../../ui/Alert/Alert";
import ProfileBookmakersSettings from "./ProfileBookmakersSettings/ProfileBookmakersSettings";

const bkOptions = [
  {
    key: "1",
    value: "1",
    text: "10bet"
  },
  {
    key: "2",
    value: "2",
    text: "377bet"
  },
  {
    key: "3",
    value: "3",
    text: "Adjarabet"
  },
  {
    key: "4",
    value: "4",
    text: "Etoto"
  },
  {
    key: "5",
    value: "5",
    text: "NetBet"
  },
  {
    key: "6",
    value: "6",
    text: "Rubet"
  },
  {
    key: "7",
    value: "7",
    text: "ComeOn"
  },
  {
    key: "8",
    value: "8",
    text: "Jenningsbet"
  },
  {
    key: "9",
    value: "9",
    text: "Mobilebet"
  }
];

const currencyOptions = [
  {
    key: "usd",
    text: "USD",
    value: "usd"
  },
  {
    key: "rur",
    text: "RUR",
    value: "rur"
  },
  {
    key: "eur",
    text: "EUR",
    value: "eur"
  },
  {
    key: "uah",
    text: "UAH",
    value: "uah"
  }
];

const roundingOptions = [
  {
    key: "1",
    text: "1",
    value: "1"
  },
  {
    key: "2",
    text: "2",
    value: "2"
  },
  {
    key: "3",
    text: "3",
    value: "3"
  },
  {
    key: "4",
    text: "4",
    value: "4"
  }
];

class ProfileBookmakers extends Component {
  state = {};
  render() {
    return (
      <div className="profile-bookmakers">
        <Alert status="fail">
          Введено недопустимое значение поля. Параметр не будет сохранен.
        </Alert>
        <form>
          <div className="table-bookmakers-head">
            <div className="tr">
              <div className="th bk">БК</div>
              <div className="th link">Ссылка</div>
              <div className="th currency">Валюта</div>
              <div className="th rounding">Округление</div>
              <div className="th commission">Комиссия</div>
              <div className="th max">Макс. сумма</div>
              <div className="th clones">Клоны</div>
            </div>
          </div>

          <div className="table-bookmakers-body">
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <Dropdown
                  defaultValue="1"
                  placeholder=""
                  fluid
                  selection
                  options={bkOptions}
                />
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.10bet.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input className="error" type="text" value="svff" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <p>
                  <strong>10bet</strong>, 377bet, Adjarabet, Etoto, NetBet,
                  Rubet, ComeOn, JenningsBet, Mobilbet
                </p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>188bet</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.188bet.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <p>188bet, RoadBet</p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>1win</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.1wuer.xyz" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <span className="empty">&mdash;</span>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>1xbet</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.1xbet.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <p>
                  <strong>1xbet</strong>, Melbet, 1xbit, 1xbit, 1xstavka
                </p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>Baltbet</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://baltplay.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Коммисия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <p>
                  <strong>Baltbet</strong>, Baltbet.ru
                </p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>Bet-At-Home</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.bet-at-home" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input className="filled" type="text" value="5.5" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <span className="empty">&mdash;</span>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>Bet365</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.bet365.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <p>
                  <strong>Bet365</strong>, Bet365 mobile
                </p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <Dropdown
                  defaultValue="1"
                  placeholder=""
                  fluid
                  selection
                  options={bkOptions}
                />
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.10bet.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input className="error" type="text" value="svff" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <p>
                  <strong>10bet</strong>, 377bet, Adjarabet, Etoto, NetBet,
                  Rubet, ComeOn, JenningsBet, Mobilbet
                </p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>188bet</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.188bet.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <p>188bet, RoadBet</p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>1win</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.1wuer.xyz" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <span className="empty">&mdash;</span>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>1xbet</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.1xbet.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <p>
                  <strong>1xbet</strong>, Melbet, 1xbit, 1xbit, 1xstavka
                </p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>Baltbet</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://baltplay.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Коммисия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <p>
                  <strong>Baltbet</strong>, Baltbet.ru
                </p>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>Bet-At-Home</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.bet-at-home" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input className="filled" type="text" value="5.5" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <span className="empty">&mdash;</span>
              </div>
            </div>
            <div className="tr">
              <div className="td bk">
                <label htmlFor="" className="mobile-label">
                  БК
                </label>
                <span>Bet365</span>
              </div>
              <div className="td link">
                <label htmlFor="" className="mobile-label">
                  Ссылка
                </label>
                <input type="text" value="https://www.bet365.com" />
              </div>
              <div className="td currency">
                <label htmlFor="" className="mobile-label">
                  Валюта
                </label>
                <Dropdown
                  defaultValue="usd"
                  placeholder=""
                  fluid
                  selection
                  options={currencyOptions}
                />
              </div>
              <div className="td rounding">
                <label htmlFor="" className="mobile-label">
                  Округление
                </label>
                <Dropdown
                  placeholder="По ум."
                  fluid
                  selection
                  options={roundingOptions}
                />
              </div>
              <div className="td commission">
                <label htmlFor="" className="mobile-label">
                  Комиссия
                </label>
                <input type="text" />
              </div>
              <div className="td max">
                <label htmlFor="" className="mobile-label">
                  Макс. сумма
                </label>
                <input type="text" />
              </div>
              <div className="td clones">
                <label htmlFor="" className="mobile-label">
                  Клоны
                </label>
                <p>
                  <strong>Bet365</strong>, Bet365 mobile
                </p>
              </div>
            </div>
          </div>
        </form>
        <ProfileBookmakersSettings/>
      </div>
    );
  }
}

export default ProfileBookmakers;
