import React, { Component } from "react";
import { Dropdown } from "semantic-ui-react";
import "./profile.bookmakers.settings.scss";

const roundingOptions = [
  {
    key: "1",
    text: "1",
    value: "1"
  },
  {
    key: "2",
    text: "2",
    value: "2"
  },
  {
    key: "3",
    text: "3",
    value: "3"
  },
  {
    key: "4",
    text: "4",
    value: "4"
  }
];

const orderOptions = [
    {
      key: "1",
      text: "Математическое ожидание",
      value: "1"
    },
    {
      key: "2",
      text: "Положительный коридор",
      value: "2"
    },
    {
      key: "3",
      text: "Отрицательный коридор",
      value: "3"
    }
  ];

class ProfileBookmakersSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="settings">
        <div className="settings-block">
          <div className="settings--item round">
            <div className="title">Округление</div>
            <Dropdown
              defaultValue="1"
              placeholder=""
              fluid
              selection
              options={roundingOptions}
            />
          </div>
          <div className="settings--item order">
            <div className="title">Порядок сортировки коридоров</div>
            <Dropdown
              defaultValue="1"
              placeholder=""
              fluid
              selection
              options={orderOptions}
            />
          </div>
          <div className="settings--item links">
                <div className="form-row checkbox">
                    <input type="checkbox" id="hide-links" name="hide-links" />
                    <label htmlFor="hide-links">Не отображать ссылки</label></div>
            </div>
        </div>
        <button className="btn save-btn">Сохранить</button>
      </div>
    );
  }
}

export default ProfileBookmakersSettings;
