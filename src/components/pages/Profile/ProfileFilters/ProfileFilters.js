import React, { Component } from "react";
import "./profile.filters.scss";

import { Label, Icon, Tab, Menu } from "semantic-ui-react";

const panes = [
  {
    menuItem: (
      <Menu.Item key="1"> 
        Live-вилки<Label>4</Label>
      </Menu.Item>
    ),
    render: () => (
      <Tab.Pane>
        <div className="profile-filters--table">
          <table>
            <thead>
              <tr>
                <th className="th-check" />
                <th className="th-name">Название</th>
                <th className="th-properties">Свойства</th>
                <th className="th-settings">Значения и настройки</th>
                <th className="th-delete" />
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="td-check">
                  <div className="form-row checkbox">
                    <input
                      type="checkbox"
                      id="check-1"
                      name="check-1"
                      checked
                    />
                    <label htmlFor="check-1" />
                  </div>
                </td>
                <td className="td-name">Только футбол на победы до 5 с</td>
                <td className="td-properties">
                  <span className="color yellow">Жёлтый</span>
                  <p className="text">Громкий бас</p>
                </td>
                <td className="td-settings">
                  <Label as="button">
                    Футбол победы
                    <Icon name="delete" />
                  </Label>
                  <Label as="button">
                    Возраст от 0 до 5 с
                    <Icon name="delete" />
                  </Label>
                  <Label as="button">
                    Исключая 1xbet
                    <Icon name="delete" />
                  </Label>
                  <Label as="button">+10</Label>
                </td>
                <td className="td-delete">
                  <button className="delete-btn"><span>Удалить фильтр</span></button>
                </td>
              </tr>
              <tr>
                <td className="td-check">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="check-2" name="check-2" />
                    <label htmlFor="check-2" />
                  </div>
                </td>
                <td className="td-name">Теннис</td>
                <td className="td-properties">
                  <span className="color green">Зелёный</span>
                  <p className="text">Эмбиент номер два</p>
                </td>
                <td className="td-settings">
                  <Label as="button">
                    Теннис: все ставки
                    <Icon name="delete" />
                  </Label>
                </td>
                <td className="td-delete">
                  <button className="delete-btn"><span>Удалить фильтр</span></button>
                </td>
              </tr>
              <tr>
                <td className="td-check">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="check-3" name="check-3" />
                    <label htmlFor="check-3" />
                  </div>
                </td>
                <td className="td-name">Williams Hill</td>
                <td className="td-properties">
                  <span className="color red">Красный</span>
                  <p className="text">Громкий бас</p>
                </td>
                <td className="td-settings">
                  <Label as="button">
                    Только БК Williams Hill
                    <Icon name="delete" />
                  </Label>
                </td>
                <td className="td-delete">
                  <button className="delete-btn"><span>Удалить фильтр</span></button>
                </td>
              </tr>
              <tr>
                <td className="td-check">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="check-4" name="check-4" />
                    <label htmlFor="check-4" />
                  </div>
                </td>
                <td className="td-name">Только Vbet</td>
                <td className="td-properties">
                  <span className="color">Без цвета</span>
                  <p className="text">Эмбиент номер один</p>
                </td>
                <td className="td-settings">
                  <Label as="button">
                    Только БК Vbet
                    <Icon name="delete" />
                  </Label>
                </td>
                <td className="td-delete">
                  <button className="delete-btn"><span>Удалить фильтр</span></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </Tab.Pane>
    )
  },
  {
    menuItem: (
      <Menu.Item key="2">
        Коридоры<Label>2</Label>
      </Menu.Item>
    ),
    render: () => (
      <Tab.Pane>
        <div className="profile-filters--table">
          <table>
            <thead>
              <tr>
                <th className="th-check" />
                <th className="th-name">Название</th>
                <th className="th-properties">Свойства</th>
                <th className="th-settings">Значения и настройки</th>
                <th className="th-delete" />
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="td-check">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="check-5" name="check-5" />
                    <label htmlFor="check-5" />
                  </div>
                </td>
                <td className="td-name">Теннис</td>
                <td className="td-properties">
                  <span className="color green">Зелёный</span>
                  <p className="text">Эмбиент номер два</p>
                </td>
                <td className="td-settings">
                  <Label as="button">
                    Теннис: все ставки
                    <Icon name="delete" />
                  </Label>
                </td>
                <td className="td-delete">
                  <button className="delete-btn"><span>Удалить фильтр</span></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </Tab.Pane>
    )
  },
  {
    menuItem: (
      <Menu.Item key="3">
        Valuebets<Label>11</Label>
      </Menu.Item>
    ),
    render: () => (
      <Tab.Pane>
        <div className="profile-filters--table">
          <table>
            <thead>
              <tr>
                <th className="th-check" />
                <th className="th-name">Название</th>
                <th className="th-properties">Свойства</th>
                <th className="th-settings">Значения и настройки</th>
                <th className="th-delete" />
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="td-check">
                  <div className="form-row checkbox">
                    <input type="checkbox" id="check-6" name="check-6" />
                    <label htmlFor="check-6" />
                  </div>
                </td>
                <td className="td-name">Williams Hill</td>
                <td className="td-properties">
                  <span className="color red">Красный</span>
                  <p className="text">Громкий бас</p>
                </td>
                <td className="td-settings">
                  <Label as="button">
                    Только БК Williams Hill
                    <Icon name="delete" />
                  </Label>
                </td>
                <td className="td-delete">
                  <button className="delete-btn"><span>Удалить фильтр</span></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </Tab.Pane>
    )
  }
];

class ProfileFilters extends Component {
  state = {};
  render() {
    return (
      <div className="profile-filters">
        <form>
          <button className="btn new-filter-btn">Новый фильтр</button>
          <Tab panes={panes} />
        </form>
      </div>
    );
  }
}

export default ProfileFilters;
