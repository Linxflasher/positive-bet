import React, { Component } from "react";
import { Tab } from "semantic-ui-react";
import "./profile.scss";

import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

import ProfileUser from "./ProfileUser/ProfileUser";
import ProfileSubscription from "./ProfileSubscription/ProfileSubscription";
import ProfileCurrency from "./ProfileCurrency/ProfileCurrency";
import ProfileBookmakers from "./ProfileBookmakers/ProfileBookmakers";
import ProfileFilters from "./ProfileFilters/ProfileFilters";

const panes = [
  {
    menuItem: "Профиль",
    render: () => (
      <Tab.Pane>
        <ProfileUser />
      </Tab.Pane>
    )
  },
  {
    menuItem: "Подписка",
    render: () => (
      <Tab.Pane>
        <ProfileSubscription />
      </Tab.Pane>
    )
  },
  {
    menuItem: "Букмекеры",
    render: () => (
      <Tab.Pane>
        <ProfileBookmakers />
      </Tab.Pane>
    )
  },
  {
    menuItem: "Фильтры",
    render: () => (
      <Tab.Pane>
        <ProfileFilters />
      </Tab.Pane>
    )
  },
  {
    menuItem: "Курсы валют",
    render: () => (
      <Tab.Pane>
        <ProfileCurrency />
      </Tab.Pane>
    )
  }
];

class Profile extends Component {
  constructor() {
    super();

    this.state = {
      prevScrollpos: document.body.scrollTop,
      visible: true
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = e => {
    const { prevScrollpos } = this.state;
    const currentScrollPos = window.scrollY;
    if (currentScrollPos > prevScrollpos) {
      this.setState({
        visible: false
      });
    } else {
      this.setState({
        visible: true
      });
    }
  };

  render() {
    return (
      <div
        className={
          this.state.visible ? "page-wrapper" : "page-wrapper scrolling"
        }
      >
        <Header />
        <div className="page-profile">
          <h1>Кабинет пользователя</h1>

          <Tab panes={panes} />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Profile;
