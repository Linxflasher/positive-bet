import React, { Component } from "react";
import "./profile.currency.scss";

class ProfileCurrency extends Component {
  state = {};
  render() {
    return (
      <div className="profile-currency">
        <form>
          <div className="profile-currency--toggler form-row checkbox">
            <input type="checkbox" id="central-bank" name="central-bank" />
            <label htmlFor="central-bank">Использовать курс по ЦБ</label>
          </div>

          <div className="profile-currency--tables">
            <table className="currency-table">
              <thead>
                <tr>
                  <th className="th-currency">Валюта</th>
                  <th className="th-default">По умолчанию</th>
                  <th className="th-rate">Курс</th>
                  <th className="th-desc">Описание</th>
                </tr>
              </thead>
              <tbody>
                <tr className="active">
                  <td className="td-currency">RUR</td>
                  <td className="td-default">
                    <div className="form-row radio">
                      <input type="radio" id="rur" name="cur" />
                      <label htmlFor="rur"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="1.00000000" />
                  </td>
                  <td className="td-desc">Российский рубль (&#8381;)</td>
                </tr>
                <tr>
                  <td className="td-currency">USD</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="usd" name="cur" />
                      <label htmlFor="usd"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="64.63140000" />
                  </td>
                  <td className="td-desc">Американский доллар (&#36;)</td>
                </tr>
                <tr>
                  <td className="td-currency">EUR</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="eur" name="cur" />
                      <label htmlFor="eur"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="73.01410000" />
                  </td>
                  <td className="td-desc">Евро (&#8364;)</td>
                </tr>
                <tr>
                  <td className="td-currency">UAH</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="uah" name="cur" />
                      <label htmlFor="uah"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="2.44816000" />
                  </td>
                  <td className="td-desc">Украинская гривна (&#8372;)</td>
                </tr>
                <tr>
                  <td className="td-currency">PLN</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="pln" name="cur" />
                      <label htmlFor="pln"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="17.13590000" />
                  </td>
                  <td className="td-desc">Польский злотый (zł)</td>
                </tr>
                <tr>
                  <td className="td-currency">KZT</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="kzt" name="cur" />
                      <label htmlFor="kzt"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="0.16817100" />
                  </td>
                  <td className="td-desc">Казахский тенге (&#8376;)</td>
                </tr>
                <tr>
                  <td className="td-currency">BYN</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="byn" name="cur" />
                      <label htmlFor="byn"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="31.22590000" />
                  </td>
                  <td className="td-desc">Белорусский рубль (Br)</td>
                </tr>
                <tr>
                  <td className="td-currency">GBP</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="gbp" name="cur" />
                      <label htmlFor="gbp"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="81.926800000" />
                  </td>
                  <td className="td-desc">Английский фунт (&#163;)</td>
                </tr>
              </tbody>
            </table>

            <table className="currency-table">
              <thead>
                <tr>
                  <th className="th-currency">Валюта</th>
                  <th className="th-default">По умолчанию</th>
                  <th className="th-rate">Курс</th>
                  <th className="th-desc">Описание</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="td-currency">AMD</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="amd" name="cur" />
                      <label htmlFor="amd"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="1.0000000" />
                  </td>
                  <td className="td-desc">Армянский драм (&#1423;)</td>
                </tr>
                <tr>
                  <td className="td-currency">AUD</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="aud" name="cur" />
                      <label htmlFor="aud"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="1.0000000" />
                  </td>
                  <td className="td-desc">Австралийский доллар (A&#36;)</td>
                </tr>
                <tr>
                  <td className="td-currency">AZN</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="azn" name="cur" />
                      <label htmlFor="azn"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="1.0000000" />
                  </td>
                  <td className="td-desc">Азербайджанский манат (&#8380;)</td>
                </tr>
                <tr>
                  <td className="td-currency">MDL</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="mdl" name="cur" />
                      <label htmlFor="mdl"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="1.0000000" />
                  </td>
                  <td className="td-desc">Молдавский лей (L)</td>
                </tr>
                <tr>
                  <td className="td-currency">CAD</td>
                  <td className="td-default">
                  <div className="form-row radio">
                      <input type="radio" id="cad" name="cur" />
                      <label htmlFor="cad"></label>
                    </div>
                  </td>
                  <td className="td-rate">
                    <input type="text" value="1.0000000" />
                  </td>
                  <td className="td-desc">Канадский доллар (C&#36;)</td>
                </tr>
              </tbody>
            </table>
          </div>
        </form>
      </div>
    );
  }
}

export default ProfileCurrency;
