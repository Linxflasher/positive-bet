import React from "react";
import "./profile.subscription.table.scss";

const ProfileSubscriptionTable = () => (
  <div className="profile-subscription--table">
    <h4>Мои подписки</h4>
    <div className="sub-grid-top">
      <div className="title">Период/план</div>
      <div className="title">Дата начала</div>
      <div className="title">Дата окончания</div>
      <div className="title">Дата оплаты</div>
      <div className="title empty" />
      <div className="title">Приостановки</div>
    </div>
    <div className="sub-grid-bottom">
      <div className="row">
        <div className="text">1 месяц</div>
        <div className="text">01.05.2019 20:44</div>
        <div className="text">31.05.2019 20:44</div>
        <div className="text">01.05.2019 21:02</div>
        <div className="text empty" />
        <div className="text">0</div>
      </div>
      <div className="row">
        <div className="text">2 месяца</div>
        <div className="text">01.05.2019 20:44</div>
        <div className="text">31.05.2019 20:44</div>
        <div className="text">01.05.2019 21:02</div>
        <div className="text empty" />
        <div className="text">1</div>
      </div>
    </div>

    {/* table-modile отображаем в мобильной версии с 600px, когда верхний блок уже не получается адаптировать */}
    <table className="table-mobile">
      <tbody>
        <tr>
          <td className="title">Период/план</td>
          <td className="info">1 месяц</td>
        </tr>
        <tr>
          <td className="title">Дата начала</td>
          <td className="info">01.05.2019 20:44</td>
        </tr>
        <tr>
          <td className="title">Дата окончания</td>
          <td className="info">31.05.2019 20:44</td>
        </tr>
        <tr>
          <td className="title">Дата оплаты</td>
          <td className="info">01.05.2019 21:02</td>
        </tr>
        <tr>
          <td className="title">Приостановки</td>
          <td className="info">1</td>
        </tr>
      </tbody>
    </table>

    <table className="table-mobile">
      <tbody>
        <tr>
          <td className="title">Период/план</td>
          <td className="info">2 месяца</td>
        </tr>
        <tr>
          <td className="title">Дата начала</td>
          <td className="info">01.05.2019 20:44</td>
        </tr>
        <tr>
          <td className="title">Дата окончания</td>
          <td className="info">31.05.2019 20:44</td>
        </tr>
        <tr>
          <td className="title">Дата оплаты</td>
          <td className="info">01.05.2019 21:02</td>
        </tr>
        <tr>
          <td className="title">Приостановки</td>
          <td className="info">2</td>
        </tr>
      </tbody>
    </table>
  </div>
);

export default ProfileSubscriptionTable;
