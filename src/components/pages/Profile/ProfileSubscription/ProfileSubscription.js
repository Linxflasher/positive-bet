import React, { Component } from "react";
import { Modal } from "semantic-ui-react";
import "./profile.subscription.scss";

import ProfileSubscriptionTable from "./ProfileSubscriptionTable/ProfileSubscriptionTable";
import ProfileSubscriptionTariffs from "./ProfileSubscriptionTariffs/ProfileSubscriptionTariffs";
import TariffsMobileCarousel from "../../Tariffs/TariffsMobileCarousel/TariffsMobileCarousel";
import icBall from "./ic-ball.svg";

class ProfileSubscription extends Component {
  constructor(props) {
    super(props);
    this.state = { isModalOpened: false };

    this.handleModal = this.handleModal.bind(this);
  }

  handleModal(e) {
    e.preventDefault();
    this.setState(state => ({
      isModalOpened: !state.isModalOpened
    }));
  }

  render() {
    return (
      <div className="profile-subscription">
        <Modal className="sub-plus" open={this.state.isModalOpened}>
          <div className="subscribe-plus">
            <div className="subscribe-plus--image">
            </div>
            <div className="subscribe-plus--info">
              <h4>Перейти на Подписку+</h4>
              <div className="date current">
                <p>Текущая дата окончания подписки:</p>
                <span>17 июля 2019 года в 12:35</span>
              </div>
              <div className="date new">
                <p>Новая дата окончания подписки: </p>
                <span>7 июля 2019 года в 9:11</span>
              </div>
              <div className="advantages">
                <h5>Преимущества Подписки +</h5>
                <ul>
                  <li>Использование плагина Betextension</li>
                  <li>Использование клиентского приложения The Forks</li>
                </ul>
              </div>
              <button className="btn">Перейти</button>
              <button className="text-btn" onClick={this.handleModal}>Отменить</button>
            </div>
          </div>
        </Modal>

        <div className="profile-subscription-top">
          {/* Подписка. Добавляем класс paused при приостановке подписки */}
          <div className="profile-subscription--active paused">
            <h2>Активная подписка&nbsp;+</h2>
            <div className="plan">План подписки: 1 месяц</div>
            <div className="till">
              до 4 июля 2019<span>осталось 25 дней</span>
            </div>
            <p>
              <strong>Подписка+</strong> позволяет пользоваться услугами сайта
              positivebet.com и предоставляет данные для клиентского приложения.
            </p>
            <div className="btn-group">
              <button className="btn prolong-btn">Продлить</button>
              <button
                className="white-btn subscribe-plus-btn"
                onClick={this.handleModal}
              >
                Преобразовать в подписку&nbsp;+
              </button>
            </div>
          </div>
          {/* Подписка закончилась. */}
          {/* <div className="profile-subscription--inactive">
            <h2>Подписка закончилась</h2>
            <div className="plan">
              <img src={icBall} alt=""/>
              <span>Прошлый план: Подписка&nbsp;+ Месяц</span>
            </div>
            <div className="btn-group">
              <button className="btn now">Оформить сразу</button>
              <button
                className="white-btn tariff"
              >
                Выбрать другой тариф
              </button>
            </div>
            <p>
              <strong>Подписка+</strong> позволяет пользоваться услугами сайта
              positivebet.com и предоставляет данные для клиентского приложения.
            </p>
          </div> */}
          <div className="profile-subscription--stop">
            <div className="subscription-block">
              <div className="subscription--item">
                <h3>Приостановка 1</h3>
                <span>Продолжительность 4 дня</span>
                <div className="since">Активирована 01.06.2019</div>
              </div>
              {/* Активировать */}
              {/* <div className="subscription--item activate">
                  <h3>Приостановка 2</h3>
                  <button className="activate-btn">Активировать</button>
                </div> */}
              {/* Возобновить */}
              <div className="subscription--item resume">
                <div>
                  <h3>Приостановка 2</h3>
                  <span>Активирована 10.06.2019</span>
                </div>
                <button className="activate-btn">Возобновить</button>
              </div>
            </div>
            {/* Уведомление 1 */}
            {/* <p>
              Минимальный период приостановки &mdash; любой, максимальный период
              приостановки <strong>&mdash; 3 месяца</strong> (по истечении 3
              месяцев, подписка автоматически возобновляется или используется
              доступная приостановка)
            </p> */}
            {/* Уведомление 2 */}
            <p>
              Активная <strong>Подписка</strong> может быть преобразована в{" "}
              <strong>Подписку+</strong> только 1 раз, обратное преобразование
              не предусмотрено. Время полученной подписки сокращается
              пропорционально разнице стоимости подписок. После этого ваш
              акканут будет работать по Подписке+ до конца ее действия.
            </p>
          </div>
        </div>

        <ProfileSubscriptionTable />

        {/* Выводим этот блок, если нет подписок */}
        <ProfileSubscriptionTariffs/>
        <TariffsMobileCarousel/>
      </div>
    );
  }
}

export default ProfileSubscription;
