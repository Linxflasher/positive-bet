import React, { Component } from "react";
import "./profile.subscription.tariffs.scss";

import TariffsSubscribeToggler from "../../../Tariffs/TariffsSubscribe/TariffsSubscribeToggler/TariffsSubscribeToggler";
import TariffsList from "../../../Tariffs/TariffsList/TariffsList";

class ProfileSubscriptionTariffs extends Component {
  constructor(props) {
    super(props);
    this.state = { isToggleOn: false };

    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

  render() {
    return (
      <div className="subscribe-tariffs">
        <p>
          Вы пока не оформили подписку на наш сервис. Можете выбрать один из
          вариантов и удобный для вас срок подписки
        </p>
        <div className="subscribe-tariffs--toggle">
          <TariffsSubscribeToggler
            click={this.handleToggle}
            class={
              this.state.isToggleOn
                ? "subscription--toggler active"
                : "subscription--toggler"
            }
          />
          <p>
            <strong>Подписка</strong> позволяет пользоваться услугами сайта
            positivebet.com
            <br /> и просматривать все вилки без ограничений
          </p>
        </div>
        <TariffsList />
      </div>
    );
  }
}

export default ProfileSubscriptionTariffs;
