import React, { Component } from "react";
import "./profile.user.scss";

import Alert from "../../../ui/Alert/Alert";
import EyeHidden from "../../Auth/Icons/EyeHidden";
import EyeVisible from "../../Auth/Icons/EyeVisible";
import HintBox from "../../../ui/HintBox/HintBox";

class ProfileUser extends Component {
  constructor(props) {
    super(props);
    this.state = { isPassVisible: false };

    this.handlePassVisibility = this.handlePassVisibility.bind(this);
  }

  handlePassVisibility(event) {
    event.preventDefault();
    this.setState(state => ({
      isPassVisible: !state.isPassVisible
    }));
  }

  render() {
    return (
      <React.Fragment>
        <Alert status="success">Пароль успешно изменен.</Alert>

        <div className="profile-user">
          <div className="profile-user--parameters">
            <h2>Параметры профиля</h2>
            <div className="params-table">
              <div className="tr">
                <div className="td title">Логин</div>
                <div className="td info black">
                  <button className="copy-btn">
                    <HintBox position="bottom">
                      Скопировать ссылку в буфер обмена
                    </HintBox>
                  </button>
                  <span>adv_dev</span>
                  <button className="white-btn logout-btn">
                    Выйти из профиля
                    <HintBox position="right">
                      <h5>Выйти из профиля</h5>
                      <p>
                        Выход из профиля приведет к потере всех связанных с ним
                        сообщений, контактов и других данных.
                      </p>
                    </HintBox>
                  </button>
                </div>
              </div>
              <div className="tr">
                <div className="td title">Электроная почта</div>
                <div className="td info black">
                  <button className="copy-btn">
                    <HintBox position="bottom">
                      Скопировать ссылку в буфер обмена
                    </HintBox>
                  </button>
                  <span>adv_dev@advdev.ru</span>
                </div>
              </div>
              <div className="tr">
                <div className="td title">Дата регистрации</div>
                <div className="td info">2019.04.29 11:25:33</div>
              </div>
              <div className="tr">
                <div className="td title">Последний визит</div>
                <div className="td info">2019.06.29 17:20:29</div>
              </div>
              <div className="tr">
                <div className="td title">Статус</div>
                <div className="td info green">Активирован</div>
              </div>
              <div className="tr">
                <div className="td title">Идентификатор пользователя</div>
                <div className="td info black">
                  <button className="copy-btn">
                    <HintBox position="bottom">
                      Скопировать ссылку в буфер обмена
                    </HintBox>
                  </button>
                  <span>5cc6b4fd20443</span>
                </div>
              </div>
              <div className="tr">
                <div className="td title">Приглашён партнёром</div>
                <div className="td info black">
                  <button className="copy-btn">
                    <HintBox position="bottom">
                      Скопировать ссылку в буфер обмена
                    </HintBox>
                  </button>
                  <span>e306d</span>
                </div>
              </div>
              <div className="tr">
                <div className="td title">IP</div>
                <div className="td info">128.70.93.28 RU</div>
              </div>
            </div>
          </div>
          <div className="profile-user--pass">
            <div className="profile-user--pass--toggler">
              <h2>Сменить пароль</h2>
              <button
                className={
                  this.state.isPassVisible
                    ? "pass-toggler opened"
                    : "pass-toggler"
                }
                onClick={this.handlePassVisibility}
              >
                <EyeHidden />
                <EyeVisible />
              </button>
            </div>
            <form>
              <div className="input-rows error">
                <div className="form-row">
                  <input
                    type={this.state.isPassVisible ? "text" : "password"}
                    placeholder="Старый пароль"
                  />
                </div>
                <div className="form-row password-text error">
                  <input
                    type={this.state.isPassVisible ? "text" : "password"}
                    placeholder="Новый пароль"
                    value="a2F3kf0934"
                  />
                  <span>Мин. 4 символа</span>
                </div>
                <div className="form-row password-text error">
                  <input
                    type={this.state.isPassVisible ? "text" : "password"}
                    placeholder="Новый пароль, повтор"
                    value="a2F3kf0934"
                  />
                  <span>Мин. 4 символа</span>
                </div>

                <div className="error-msg">Введеные пароли не совпадают</div>
              </div>

              <button className="submit-btn disabled" type="submit">
                Отправить
              </button>
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ProfileUser;
