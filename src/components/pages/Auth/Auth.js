import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Modal } from "semantic-ui-react";
import Close from "./Icons/Close";
import EyeHidden from "./Icons/EyeHidden";
import EyeVisible from "./Icons/EyeVisible";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = { isPassVisible: false, isModalRecOpened: false };

    this.handlePassVisibility = this.handlePassVisibility.bind(this);
    this.handleModalRecovery = this.handleModalRecovery.bind(this);
  }

  handlePassVisibility(event) {
    event.preventDefault();
    this.setState(state => ({
      isPassVisible: !state.isPassVisible
    }));
  }

  handleModalRecovery(e) {
    e.preventDefault();
    this.setState(state => ({
      isModalRecOpened: !state.isModalRecOpened
    }));
  }

  render() {
    return (
      <React.Fragment>
        {/* Login modal */}
        <Modal open={this.props.openLogin}>
            <h2 id="modal-title" className="modal--title">
              Войдите в свой аккаунт
            </h2>
            <button className="modal--close" onClick={this.props.closeLogin}>
              <Close />
            </button>

            <form className="form-login" action="">
              <div className="input-rows">
                <div className="row">
                  <input type="email" placeholder="E-mail" />
                </div>
                <div className="row password">
                  <input
                    type={this.state.isPassVisible ? "text" : "password"}
                    placeholder="Пароль"
                  />
                  <button
                    className={
                      this.state.isPassVisible
                        ? "pass-toggler opened"
                        : "pass-toggler"
                    }
                    onClick={this.handlePassVisibility}
                  >
                    <EyeHidden />
                    <EyeVisible />
                  </button>
                </div>

                <div className="error-msg">Неправильный E-mail или пароль</div>
              </div>
              <button
                className="submit-btn disabled"
                type="submit"
              >
                Войти
              </button>
              <div className="row help-links">
                <Link className="register-link" to="/">
                  Зарегистрироваться
                </Link>
                <Link
                  id="recover-link"
                  to="/"
                  onClick={this.handleModalRecovery}
                >
                  Забыли пароль?
                </Link>
              </div>
            </form>
        </Modal>
        {/* Login modal end */}

        {/* Registration modal */}
        <Modal open={this.props.openReg}>
            <h2 id="modal-title" className="modal--title">
              Регистрация
            </h2>
            <button className="modal--close" onClick={this.props.closeReg}>
              <Close />
            </button>

            <form className="form-reg" action="">
              <div className="input-rows error">
                <div className="row">
                  <input
                    className="filled"
                    type="email"
                    placeholder="E-mail"
                    value="test@ya.ru"
                  />
                </div>
                <div className="row password-text error">
                  <input
                    type="password"
                    value="vfdgreegrgeg"
                    placeholder="Пароль"
                  />
                  <span>Мин. 4 символа</span>
                </div>
                <div className="row password-text">
                  <input type="password" placeholder="Повторите Пароль" />
                </div>
                <div className="error-msg">
                  Неправильный формат E-mail
                  <br />
                  Введённые пароли не совпадают
                </div>
              </div>

              <div className="agreement">
                <div className="row checkbox">
                  <input type="checkbox" id="user-agr" name="user-agr" />
                  <label htmlFor="user-agr">
                    Я принимаю условия&nbsp;
                    <Link to="/">Пользовательского соглашения</Link>
                  </label>
                </div>
                <div className="row checkbox">
                  <input type="checkbox" id="data-agr" name="data-agr" />
                  <label htmlFor="data-agr">
                    Я даю своё согласие на сбор, обработку и хранение моих
                    персональных данных
                  </label>
                </div>
              </div>

              <button className="submit-btn disabled" type="submit">
                Зарегистрироваться
              </button>
              <Link id="login-link" className="bottom-link" to="/">
                У меня уже есть аккаунт
              </Link>
            </form>
        </Modal>
        {/* Registration modal end */}

        {/* Recovery modal */}
        <Modal open={this.state.isModalRecOpened}>
            <h2 id="modal-title" className="modal--title">
              Восстановление пароля
            </h2>
            <button className="modal--close" onClick={this.handleModalRecovery}>
              <Close />
            </button>

            <form className="form-recovery" action="">
              <div className="input-rows recover-email error">
                <div className="row error">
                  <input
                    type="email"
                    id="email-rec"
                    name="email-rec"
                    placeholder="E-mail"
                  />
                  <label htmlFor="email-rec">
                    Введите адрес электронной почты указанный при регистрации
                  </label>
                </div>
                <div className="error-msg">
                  Пользователь с таким адресом email не зарегистрирован на сайте
                </div>
              </div>

              <button className="submit-btn" type="submit">
                Восстановить
              </button>
              <Link className="bottom-link register-link" to="/">
                Зарегистрироваться
              </Link>
            </form>
        </Modal>
        {/* Recovery modal end */}
      </React.Fragment>
    );
  }
}

export default Auth;
