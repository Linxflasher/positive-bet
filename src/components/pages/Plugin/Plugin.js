import React from "react";
import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

const Plugin = () => (
  <div className="page-wrapper">
    <Header />
    <div className="text-page">
      <h1>Расширение Positivebet</h1>
      <p>
        Расширение доступно для браузеров Chrome, Яндекс, Opera, Firefox.
        Упрощает процесс ставки: открывает событие, купон и вводит сумму ставки
        из калькулятора.
      </p>

      <div className="text-page--item">
        <h2>В настоящее время поддерживается следующий список контор:</h2>
        <ul className="companies-list">
          <li>10bet</li>
          <li>betcity</li>
          <li>favbet</li>
          <li>matchbook</li>
          <li>pinnacle</li>
          <li>vbet</li>
          <li>1xbet</li>
          <li>888sport</li>
          <li>betfair</li>
          <li>fonbet</li>
          <li>melbet</li>
          <li>sbobet</li>
          <li>1xbet</li>
          <li>betfair</li>
          <li>fonbet</li>
          <li>melbet</li>
          <li>sbobet</li>
          <li>1xbet</li>
          <li>888sport</li>
          <li>betfair sport</li>
          <li>gamebookers</li>
          <li>mostbet</li>
          <li>parimatch</li>
          <li>vulkan</li>
          <li>baltbet</li>
          <li>betrally</li>
          <li>leon</li>
          <li>olimp</li>
          <li>sportingbet</li>
          <li>willhill</li>
          <li>bet-at-home</li>
          <li>bwin</li>
          <li>liga stavok</li>
          <li>paddypower</li>
          <li>sportingbull</li>
          <li>winline</li>
          <li>bet365</li>
          <li>dafabet</li>
          <li>marathon</li>
          <li>parimatch</li>
          <li>tennisi</li>
          <li>zenit</li>
        </ul>
      </div>

      <div className="text-page--item">
        <h2>ЦУПИСные версии сайтов поддерживаются для следющих контор:</h2>
        <ul className="companies-list">
          <li>baltbet</li>
          <li>bwin</li>
          <li>leon</li>
          <li>ligastavok</li>
          <li>parimatch</li>
          <li>1xstavka</li>
          <li>betcity</li>
          <li>fonbet</li>
          <li>olimp</li>
          <li>marathonbet</li>
          <li>winline</li>
          <li>tennisi</li>
          <li>zenit</li>
        </ul>
        <p>
          Расширение работает при активной подписке. В браузере должны быть
          включены javascript и cookie.
          <br />
          Расширение добавляет в калькуляторе кнопки "Перейти" только для
          поддерживаемых контор.
        </p>
        <p>
          Установить расширение можно из маркета или вручную. Устрановка из
          маркета временно не поддерживается.
          <br />В случае ручной установки, возможно потребуется удалять и
          устанавливать расширение заново, каждый раз после закрытия браузера.
        </p>
      </div>

      <div className="text-page--item">
        <h2>Установка в браузер Яндекс, Opera.</h2>
        <ul className="pad-list">
          <li>
            Скачайте zip архив{" "}
            <a href="/" target="_blank">
              positivebet_yandex_opera.zip (версия 3.1.17)
            </a>{" "}
            и сохраните на диск.
          </li>
          <li>
            Извлеките файл positivebet_yandex_opera.crx из архива и сохраните
            его на диск.
            <br />
            Если не сделать этот шаг, возможна ошибка "Файл не найден".
          </li>
          <li>
            Откройте в браузере список всех расширений (browser://extensions/).
          </li>
          <li>Удалите старые расширения positivebet, если они есть.</li>
          <li>
            Перетащите ранее распакованный и сохраненный на диск файл
            positivebet_yandex_opera.crx на эту страницу со списком расширений.
          </li>
          <li>Нажмите кнопку подтверждения установки.</li>
          <li>Обновите страницу сканера Ctrl + F5 или Ctrl + R или Cmd + R.</li>
        </ul>
      </div>
      <div className="text-page--item">
        <h2>Установка в браузер Mozilla Firefox.</h2>
        <ul className="pad-list">
          <li>
            Скачайте zip архив{" "}
            <a href="/" target="_blank">
              positivebet_firefox.zip (версия 3.1.16)
            </a>{" "}
            и сохраните на диск.
          </li>
          <li>
            Извлеките файл positivebet_firefox.xpi из архива и сохраните его на
            диск.
            <br />
            Если не сделать этот шаг, возможна ошибка "Файл не найден".
          </li>
          <li>Откройте в браузере список всех расширений (about:addons).</li>
          <li>Удалите старые расширения positivebet, если они есть.</li>
          <li>
            Нажмите кнопку "Шестеренка", выберите "Установить дополнение из
            файла" и укажите путь до ранее распакованного файла
            positivebet_firefox.xpi.
          </li>
          <li>Нажмите кнопку подтверждения установки.</li>
          <li>Обновите страницу сканера Ctrl + F5 или Ctrl + R или Cmd + R.</li>
        </ul>
      </div>
      <div className="text-page--item">
        <h2>Установка в браузер Google Chrome.</h2>
        <ul className="pad-list">
          <li>
            Скачайте zip архив{" "}
            <a href="/" target="_blank">
              positivebet_chrome.zip (версия 3.1.17)
            </a>{" "}
            и сохраните на диск.
          </li>
          <li>
            Извлеките папку positivebet_chrome из архива и сохраните ее на диск.
          </li>
          <li>
            Откройте в браузере список всех расширений (chrome://extensions/).
          </li>
          <li>Включите "Режим разработчика".</li>
          <li>Удалите старые расширения positivebet, если они есть.</li>
          <li>
            Нажмите кнопку "Загрузить распакованное расширение" и укажите путь
            до ранее распакованной папки positivebet_chrome.
          </li>
          <li>Нажмите кнопку подтверждения установки.</li>
          <li>Обновите страницу сканера Ctrl + F5 или Ctrl + R или Cmd + R.</li>
        </ul>
      </div>
    </div>
    <Footer />
  </div>
);

export default Plugin;
