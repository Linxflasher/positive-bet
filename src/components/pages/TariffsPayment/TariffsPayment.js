import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./tariffs.payment.scss";
import ballImg from "./ball.svg";
import icClock from "./ic-clock.svg";

import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

class TariffsPayment extends Component {
  state = {};
  render() {
    return (
      <div className="page-wrapper">
        <Header />
        <div className="tarif-page">
          <div className="back-link">
            <Link to="/tariffs">Вернуться к выбору тарифа</Link>
          </div>
          <div className="subscribe-block-wrapper">
            <div className="subscribe-block">
              <div className="subscribe-block--desc">
                <h1>Подписка+ на месяц</h1>
                <p>Приостановки - 3</p>
                <p>Максимум 3 месяца</p>
              </div>
              <div className="subscribe-block--price">
                <p className="new">89</p>
                <p className="prev">99</p>
                <img className="ball" src={ballImg} alt="" />
              </div>
            </div>
          </div>
          <div className="subscribe--payment-details">
            <div className="left">
              <p>
              Выберите удобный способ оплаты: 
              </p>
            </div>
            <div className="right">
              <p>
                Платёж может поступить с заддержкой,
                <br />в пределах нескольких минут
              </p>
              <img src={icClock} alt="" />
            </div>
          </div>
          <div className="payment-list">
            <Link to="/operator" className="payment--item qiwi" />
            <Link to="/operator" className="payment--item yandex" />
            <Link to="/operator" className="payment--item visa" />
            <Link to="/operator" className="payment--item webmoney processing" />
            <Link to="/operator" className="payment--item skrill" />
            <Link to="/operator" className="payment--item neteller" />
            <Link to="/operator" className="payment--item processing">
              <span>Другие</span>
              </Link>
          </div>
          <div className="mobile-notice">
              <img src={icClock} alt="" />
              <p>
                Платёж может поступить с заддержкой,
                <br />в пределах нескольких минут
              </p>
            </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default TariffsPayment;
