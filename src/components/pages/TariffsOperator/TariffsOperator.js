import React, { Component } from "react";
import { Link } from "react-router-dom";
import ballImg from "../TariffsPayment/ball.svg";
import icClock from "../TariffsPayment/ic-clock.svg";
import QiwiLogo from "./logo-qiwi-white.svg";

import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

class TariffsOperator extends Component {
  state = {};
  render() {
    return (
      <div className="page-wrapper">
        <Header />
        <div className="tarif-page operator">
          <div className="back-link">
            <Link to="/payment">Вернуться к выбору способа оплаты</Link>
          </div>
          <div className="subscribe-block-wrapper">
            <div className="subscribe-block">
              <div className="subscribe-block--desc">
                <img src={QiwiLogo} alt="" />
              </div>
              <div className="subscribe-block--price">
                <p className="new">89</p>
                <p className="prev">99</p>
                <img className="ball" src={ballImg} alt="" />
              </div>
            </div>
          </div>
          <div className="subscribe--payment-details">
            <div className="left">
              <p>
              Выберите подходящего вам операторатора:
              </p>
            </div>
          </div>
          <div className="payment-list">
            <Link to="/" className="payment--item qiwi" />
            <Link to="/" className="payment--item sprypay" />
            <Link to="/" className="payment--item onpay" />
            <Link to="/" className="payment--item pokupo" />
          </div>
          <div className="mobile-notice">
              <img src={icClock} alt="" />
              <p>
                Платёж может поступить с заддержкой,
                <br />в пределах нескольких минут
              </p>
            </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default TariffsOperator;
