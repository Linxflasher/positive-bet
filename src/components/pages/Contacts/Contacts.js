import React, { Component } from 'react';
import "./contacts.scss"
import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

import ContactsForm from "./ContactsForm/ContactsForm";

class Contacts extends Component {
    state = {  }
    render() {
        return (
            <div className="page-wrapper">
                <Header/>
                <div className="contacts-page">
                    <h1>Обратная связь</h1>
                    <p>Если у вас есть вопросы или пожелания пожалуйста свяжитесь с нашей службой поддержки.</p>

                    <div className="phone-email">
                        <h2>Горячая линия</h2>
                        <p>Электронная почта: <a className="email" href="mailto:support@positivebet.com">support@positivebet.com</a></p>
                        <p>Телефон: <a className="phone" href="tel:+79688529113">+7 (968) 852-91-13</a> (08:00−15:00 по Москве)</p>
                    </div>

                    <ContactsForm/>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Contacts;