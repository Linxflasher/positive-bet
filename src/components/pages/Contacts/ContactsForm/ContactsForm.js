import React, { Component } from "react";
import "./contacts.form.scss";
import { Dropdown } from "semantic-ui-react";
import Png from "./FileIcons/Png";
import Jpg from "./FileIcons/Jpg";
import Gif from "./FileIcons/Gif";
import Pdf from "./FileIcons/Pdf";
import Delete from "./FileIcons/Delete";

const subjectOptions = [
  { value: "1", text: "Авторизация" },
  { value: "2", text: "Плагин" },
  { value: "3", text: "Регистрация" },
  { value: "4", text: "Покупка подписки" },
  { value: "5", text: "Другая тема" },
  { value: "6", text: "Своя тема" }
];

class ContactsForm extends Component {
  state = {};
  render() {
    return (
      <form action="" className="contacts-form">
        <h3>Сообщение в поддержку</h3>
        <p>
          Связаться со службой поддержки можно заполнив форму обратной связи
        </p>
        <span className="required">Обязательное поле</span>
        <div className="form-row req error">
          <input
            type="email"
            id="email"
            name="email"
            value="testru"
            placeholder="E-mail"
          />
          <div className="error-msg">Неправильный формат E-mail</div>
        </div>
        <div className="form-row">
          {/* Когда в поле есть введённые символы, нужно добавлять класс .filled */}
          <input
            className="filled"
            type="text"
            id="name"
            name="name"
            value="Сергей"
            placeholder="Ваше имя"
          />
        </div>

        <div className="form-row req">
          <Dropdown
            placeholder="Выберите тему сообщения"
            selection
            options={subjectOptions}
          />
          <div className="error-msg">Обязательно для заполнения</div>
        </div>

        <div className="form-row textarea req">
          <textarea
            // Нужно вешать класс draggable, когда пользователь ресайзит textarea
            // className="draggable"
            name="message"
            id="message"
            placeholder="Текст сообщения"
            maxLength="300"
          />
          <div className="symbols-counter">0/300</div>
          <div className="error-msg">Обязательно для заполнения</div>
        </div>

        <div className="attach-file">
          <div className="desc">
            <p className="file-text">
              Прикрепить файлы <span>(10/10)</span>
            </p>
            <p className="notice">
              Можно добавить не более 10 файлов форматов gif, jpg, png, pdf
              Объем файла не более 1 мб{" "}
            </p>
          </div>
          {/* Для дизейбла кнопки вешаем на .file класс .disabled */}
          <div className="form-row file disabled">
            <input id="file" name="file" type="file" />
            <label htmlFor="file">Добавить</label>
          </div>
        </div>

        <ul className="files-list">
          <li>
            <Delete />
            <Png />
            <span>screenshot_bet</span>
          </li>
          <li>
            <Delete />
            <Gif />
            <span>screenshot</span>
          </li>
          <li>
            <Delete />
            <Pdf />
            <span>screenshot_bet_123</span>
          </li>
          <li>
            <Delete />
            <Jpg />
            <span>screenshot_bet_4</span>
          </li>
          <li>
            <Delete />
            <Png />
            <span>screenshot_bet</span>
          </li>
          <li>
            <Delete />
            <Gif />
            <span>screenshot</span>
          </li>
          <li>
            <Delete />
            <Pdf />
            <span>screenshot_bet_123</span>
          </li>
          <li>
            <Delete />
            <Jpg />
            <span>screenshot_bet_4</span>
          </li>
          <li>
            <Delete />
            <Png />
            <span>screenshot_bet</span>
          </li>
          <li>
            <Delete />
            <Gif />
            <span>screenshot</span>
          </li>
        </ul>
        <button className="submit-btn">Отправить</button>
      </form>
    );
  }
}

export default ContactsForm;
