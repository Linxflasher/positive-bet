import React from "react";

const Jpg = () => (
  <div className="file-format jpg">
    <svg
      width="40"
      height="40"
      viewBox="0 0 40 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2.5 36.25V3.75002C2.5 3.05963 3.05961 2.50002 3.75 2.50002H22.5V7.50002C22.5 8.88072 23.6193 10 25 10H30V13.75H32.5V8.75002C32.502 8.41775 32.3715 8.09838 32.1375 7.86252L24.6375 0.36252C24.4016 0.128536 24.0823 -0.00185505 23.75 1.99452e-05H3.75C1.67891 1.99452e-05 0 1.679 0 3.75002V36.25C0 38.3211 1.67891 40 3.75 40H12.5V37.5H3.75C3.05969 37.5 2.5 36.9404 2.5 36.25Z"
        fill="#4C4C4C"
      />
      <path
        d="M37.5 25H35V27.5H37.5V30H32.5V22.5H40C40 21.1193 38.8807 20 37.5 20H32.5C31.1193 20 30 21.1193 30 22.5V30C30 31.3807 31.1193 32.5 32.5 32.5H37.5C38.8807 32.5 40 31.3807 40 30V27.5C40 26.1193 38.8807 25 37.5 25Z"
        fill="#4C4C4C"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M21.25 20H26.25C27.6307 20 28.75 21.1193 28.75 22.5V26.25C28.75 27.6307 27.6307 28.75 26.25 28.75H22.5V32.5H20V21.25C20 20.5596 20.5596 20 21.25 20ZM22.5 26.25H26.25V22.5H22.5V26.25Z"
        fill="#4C4C4C"
      />
      <path
        d="M16.25 30H11.25V32.5H16.25C17.6307 32.5 18.75 31.3807 18.75 30V20H16.25V30Z"
        fill="#4C4C4C"
      />
    </svg>
  </div>
);

export default Jpg;
