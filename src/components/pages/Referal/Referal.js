import React, { Component } from "react";
import "./referal.scss";
import { Link } from "react-router-dom";
import Header from "../../template/Header/Header";
import Footer from "../../template/Footer/Footer";

import Qiwi from "./logo-qiwi.svg";
import Yandex from "./logo-yandex.svg";
import Visa from "./logo-visa.svg";
import Webmoney from "./logo-webmoney.svg";
import Skrill from "./logo-skrill.svg";

class Referal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      referalLinksVisible: false
    };

    this.handleVisibility = this.handleVisibility.bind(this);
  }

  handleVisibility() {
    const referal = document.querySelector(".referal-links-block");
    this.setState({
      referalLinksVisible: true
    });

    setTimeout(() => {
      window.scrollTo({
        behavior: "smooth",
        left: 0,
        top: referal.offsetTop
      });
    }, 200);
  }

  render() {
    return (
      <div className="page-wrapper">
        <Header />

        <main className="main-content referal-page">
          <section className="referal">
            <h1>Партнерская программа</h1>
            <p>
              Предлагаем партнёрскую программу по привлечению пользователей на
              наш сайт.
            </p>

            <h2>
              Вы размещаете рекламную ссылку на своем сайте.
              <br />
              Мы выплачиваем комиссию по следующим условиям:
            </h2>
            <ul className="referal--list">
              <li>
                если доход за месяц от привлеченных вами пользователей будет от
                $350 до $500, то ваша комиссия составит 20% от суммы
              </li>
              <li>если доход будет от $500 до $1000 - комиссия 25%</li>
              <li>если больше $1000 - комиссия 30%</li>
              <li>если больше $10000 - комиссия 40%</li>
            </ul>

            <p className="terms">
              Ограничений по максимальной выплате по клиенту нет. Месяц
              календарный. Если доход от привлеченных пользователей менее $350,
              то выплата не производится, доход не переносится на следующий
              месяц. В партнерской программе учитываются покупки{" "}
              <strong>Подписок</strong> и <strong>Подписок+</strong>. Подписки,
              отмененные пользователем, не учитываются.{" "}
            </p>
            <p>Комиссия платежной системы на вывод:</p>

            <div className="commission">
              <div className="commission--item">
                <div className="left">
                  <img src={Qiwi} alt="" />
                  <img src={Yandex} alt="" />
                  <img className="visa" src={Visa} alt="" />
                </div>
                <div className="right">3%</div>
              </div>
              <div className="commission--item">
                <div className="left">
                  <img src={Webmoney} alt="" />
                </div>
                <div className="right">0.8%</div>
              </div>
              <div className="commission--item">
                <div className="left">
                  <img src={Skrill} alt="" />
                </div>
                <div className="right">0%</div>
              </div>
            </div>

            <button className="btn referal-btn" onClick={this.handleVisibility}>
              Получить партнёрскую ссылку
            </button>

            <div
              className={
                this.state.referalLinksVisible
                  ? "referal-links-block opened"
                  : "referal-links-block"
              }
            >
              <div className="referal-links">
                <h3>Ваша реферальная ссылка</h3>
                <table>
                  <tr>
                    <td>Главная страница</td>
                    <td>
                      <div className="link">
                        <button className="copy-btn" />
                        <a href="https://positivebet.com/pid/4e883">
                          https://positivebet.com/pid/4e883
                        </a>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Страница регистрации</td>
                    <td>
                      <div className="link">
                        <button className="copy-btn" />
                        <a href="https://positivebet.com/pid/4e883/r">
                          https://positivebet.com/pid/4e883/r
                        </a>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Тарифы</td>
                    <td>
                      <div className="link">
                        <button className="copy-btn" />
                        <a href="https://positivebet.com/pid/4e883/s">
                          https://positivebet.com/pid/4e883/s
                        </a>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Тарифы Подписка+</td>
                    <td>
                      <div className="link">
                        <button className="copy-btn" />
                        <a href="https://positivebet.com/pid/4e883/es">
                          https://positivebet.com/pid/4e883/es
                        </a>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Идентификатор</td>
                    <td>
                      <div className="link">
                        <button className="copy-btn" />
                        <span>4e883</span>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>

              <Link to="/profile" className="btn partner-link">
                Партнёрская статистика
              </Link>
            </div>
          </section>
        </main>

        <Footer />
      </div>
    );
  }
}

export default Referal;
