import React, { Component } from "react";
import "./recovery.scss";
import Header from "../../template/Header/Header";
import Alert from "../../ui/Alert/Alert";

import videoPoster from "./video-poster.jpg";

class Recovery extends Component {
  state = {
    videoPlays: false
  };

  playVideo() {
    this.refs.vidRef.play();
    this.setState({
      videoplays: !this.state.videoPlays
    });
  }

  render() {
    return (
      <div className="page-wrapper">
        <Header />

        <div className="alerts-wrapper">
          <Alert status="">
            Мы используем файлы cookie, чтобы анализировать трафик, подбирать
            для вас подходящий контент. <a href="/">Подробнее</a>
          </Alert>

          <Alert status="warn">
            Внимание! Сегодня с 20:00 до 22:00 сервис будет недоступен в связи с
            плановым обновлением оборудования. Приносим извинения за неудобства.
          </Alert>

          <Alert status="success">
            На ваш адрес электронной почты было отправлено письмо с дальнейшими
            инструкциями.
          </Alert>

          <Alert status="fail">
            Не удалось сохранить фильтр. Попробуйте изменить настройки или
            повторить процедуру позднее.
          </Alert>
        </div>

        <div className="recovery">
          <div className="recovery--block">
            <div className="recovery--block--description">
              <h1>Я хочу попробовать прямо сейчас</h1>
              <p>
                Посмотрите нашу краткую инструкцию для старта работы с сканером
                вилок. Наша мини-эциклопедия будет полезна как новичку, так и
                профессиональному вилочнику.
              </p>
              <button className="check-btn">Справка</button>
            </div>
            <div className="recovery--block--video">
              <button
                onClick={this.playVideo.bind(this)}
                className={
                  this.state.videoplays
                    ? "play-video-btn playing"
                    : "play-video-btn"
                }
              />
              <video ref="vidRef" poster={videoPoster} controls>
                <source
                  src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                  type="video/mp4"
                />
                Этот браузер не поддерживает video тэг.
              </video>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Recovery;
