import React, { Component } from "react";
import { Router, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import Main from "./components/pages/Main/Main";
import Tariffs from "./components/pages/Tariffs/Tariffs";
import TariffsPayment from "./components/pages/TariffsPayment/TariffsPayment";
import TariffsOperator from "./components/pages/TariffsOperator/TariffsOperator";
import Recovery from "./components/pages/Recovery/Recovery";
import TermsOfService from "./components/pages/TermsOfService/TermsOfService";
import Plugin from "./components/pages/Plugin/Plugin";
import Questions from "./components/pages/Questions/Questions";
import Contacts from "./components/pages/Contacts/Contacts";
import ContactsSent from "./components/pages/ContactsSent/ContactsSent";
import Profile from "./components/pages/Profile/Profile";
import Referal from "./components/pages/Referal/Referal";
import LiveForks from "./components/pages/LiveForks/LiveForks";
import LiveForksFilter from "./components/pages/LiveForks/LiveForksFilter/LiveForksFilter";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Main} />
        <Route path="/tariffs" component={Tariffs} />
        <Route path="/payment" component={TariffsPayment} />
        <Route path="/operator" component={TariffsOperator} />
        <Route path="/recovery" component={Recovery} />
        <Route path="/terms" component={TermsOfService} />
        <Route path="/plugin" component={Plugin} />
        <Route path="/questions" component={Questions} />
        <Route path="/contacts" component={Contacts} />
        <Route path="/contacts-sent" component={ContactsSent} />
        <Route path="/profile" component={Profile} />
        <Route path="/referal" component={Referal} />
        <Route path="/live-forks" component={LiveForks} />
        <Route path="/live-forks-filter" component={LiveForksFilter} />
      </Switch>
    );
  }
}

const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => ({});

export default App;
